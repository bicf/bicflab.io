## 1. 整体架构

SPAUN的整体架构如图所示：

![SPAUN-ARCH](../img/neuroscience/spaun-arch.png)

具体来说，SPAUN的主要模块如下：三个层级模块，动作选择模块以及五个子系统（Information Encoding, Transformation Calculation, Reward Evaluation, Information Decoding, Motor Processing）。

## 2. 核心流程：Cortex-Basal Ganglia-Thalamus循环

Cortex-Basal Ganglia-Thalamus循环:

![SPAUN-LOOP](../img/neuroscience/spaun-loop.png)

具体流程如下：

1. Cortex产生内容上下文；
2. Basal ganglia依据内置的规则针对上下文和规则的条件进行匹配；
3. Thalamus针对Basal ganglia选择的条件对应的结果修改Cortex。

## 3. 任务描述

| 任务 | 描述 | 目标|
|---|---|---|
| Copy drawing | 重现输入的视觉细节 | 视觉信息可以保存下来 |
| Recognition | 数字分类 | 可以辨识概念，忽略扰动|
| Reinforcement learning | 可以响应回馈 | 大规模模型上的强化学习|
| Serial working memory | 记忆并重复有序集合 | 将序列记忆整合进认知模型 |
| Counting | 从给定输入数特定的数目 | 灵活决策，可以发现表示之间的联系|
| Question answering | 回答working memory中结构化表示的内容的相关问题 | 可以基于中间表示灵活地构造、操作和提取信息|
| Rapid variable creation | 识别句法模式并迅速反应 | 可以满足Hadley（2009）认为当前无法完成的认知需求|
| Fluid reasoning | Raven’s Progressive Matrices  | 重新部署认知资源|

## 4. 模型分析

| Standard Model假设 (Table 1) | 是否符合|
|---|---|
| A. Structure and Processing ||
| &emsp;1. The purpose of architectural processing is to support bounded rationality, not optimality | 是 |
| &emsp;2. Processing is based on a small number of task-independent modules | 是 |
| &emsp;3. There is significant parallelism in architectural processing | 是 |
| &emsp;&emsp;a. Processing is parallel across modules | 是 |
| &emsp;&emsp;&emsp;i. ACT-R & Soar: asynchronous; Sigma: synchronous | asynchronous |
| &emsp;&emsp;b. Processing is parallel within modules | 是 | 
| &emsp;&emsp;&emsp;i. ACT-R: rule match, Sigma: graph solution, Soar: rule firings | spkings neurons |
| &emsp;4. Behavior is driven by sequential action selection via a cognitive cycle that runs at ~50 ms per cycle in human cognition | ~150ms 且受外部输入驱动 |
| &emsp;5. Complex behavior arises from a sequence of independent cognitive cycles that operate in their local context, without a separate architectural module for global optimization (or planning). | 不确定，部分模块可以获取全局信息 |
| B. Memory and Content ||
| &emsp;1. Declarative and procedural long-term memories contain symbol structures and associated quantitative metadata | 并不区分两类存储，其它符合 |
| &emsp;&emsp;a. ACT-R: chunks with activations and rules with utilities; Sigma: predicates and conditionals with functions; Soar: triples with activations and rules with utilities | 语义指针 |
| &emsp;2. Global communication is provided by a short-term working memory across all cognitive, perceptual, and motor modules | 部分模块可以直接交互 |
| &emsp;3. Global control is provided by procedural long-term memory | 不区分两类存储，输入驱动 |
| &emsp;&emsp;a. Composed of rule-like conditions and actions | 是 |
| &emsp;&emsp;b. Exerts control by altering contents of working memory | 是 |
| &emsp;4. Factual knowledge is provided by declarative long-term memory | 不区分两类存储 |
| &emsp;&emsp;a. ACT-R: single declarative memory; Sigma: unifies with procedural memory; Soar: semantic and episodic memories | 语义指针 |
| C. Learning ||
| &emsp;1. All forms of long-term memory content, whether symbol structures or quantitative metadata, are learnable | 规则不可学习 |
| &emsp;2. Learning occurs online and incrementally, as a side effect of performance and is often based on an inversion of the flow of information from performance | 支持RL，但其它任务不需要在线学习 |
| &emsp;3. Procedural learning involves at least reinforcement learning and procedural composition | RL是,PC否 |
| &emsp;&emsp;a. Reinforcement learning yields weights over action selection | 是 |
| &emsp;&emsp;b. Procedural composition yields behavioral automatization | 无PC |
| &emsp;&emsp;&emsp;i. ACT-R: rule composition; Sigma: under development; Soar: chunking | 无PC  | 
| &emsp;4. Declarative learning involves the acquisition of facts and tuning of their metadata | 不支持 |
| &emsp;5. More complex forms of learning involve combinations of the fixed set of simpler forms of learning | 是 |
| D. Perception and Motor ||
| &emsp;1. Perception yields symbol structures with associated metadata in specific working memory buffers | 是 |
| &emsp;&emsp;a. There can be many different such perception modules, each with input from a different modality and its own buffer | 只有一个 |
| &emsp;&emsp;b. Perceptual learning acquires new patterns and tunes existing ones | Peception不变 |
| &emsp;&emsp;c. An attentional bottleneck constrains the amount of information that becomes available in working memory | 输入信息量固定 |
| &emsp;&emsp;d. Perception can be influenced by top-down information provided from working memory | 否 |
| &emsp;2. Motor control converts symbolic relational structures in its buffers into external actions | 是 |
| &emsp;&emsp;a. As with perception, there can be multiple such motor modules | 只有一个 |
| &emsp;&emsp;b. Motor learning acquires new action patterns and tunes existing ones | Motor 不变 |

## 参考文档

SPAUN 具体内容参考[PPT](https://gitlab.com/bicf/bicf.gitlab.io/tree/master/docs/attachments/neuroscience/SPAUN-20181225.pptx).
