disqus:

# 准备环境（以Windows为例）

* 下载并安装[Git](https://git-scm.com/downloads)
* 下载并安装[Anaconda](https://www.anaconda.com/download/)
* 运行“Anaconda Prompt”，打开命令行界面
* 运行命令安装所需的组件：pip install PyHamcrest mkdocs pymdown-extensions mkdocs-material
* 从Gitlab上同步最新代码：git clone https://gitlab.com/bicf/bicf.gitlab.io.git
* 在docs文件夹下的适当位置创建新的MarkDown文件，编辑mkdocs.yml配置文件，为新生成的文件添加目录索引
* 参照这里接下来的内容编写新生成的文件
* 使用命令：mkdocs serve，通过浏览本地服务：http://127.0.0.1:8000，来确认新编写的内容是正确无误的
* 将确认过的内容提交到本地Git仓库，并与GitLab仓库进行同步（同步前请先合并其他人的修改）
* 等待远端构建结束后，访问：https://bicf.gitlab.io/，做最终确认
