disqus:

# 使用本系统编写文档的说明

## 准备环境（以Windows为例）

* 下载并安装[Git](https://git-scm.com/downloads)
* 下载并安装[Anaconda](https://www.anaconda.com/download/)
* 运行“Anaconda Prompt”，打开命令行界面
* 运行命令安装所需的组件：pip install PyHamcrest mkdocs pymdown-extensions mkdocs-material
* 从Gitlab上同步最新代码：git clone https://gitlab.com/bicf/bicf.gitlab.io.git
* 在docs文件夹下的适当位置创建新的MarkDown文件，编辑mkdocs.yml配置文件，为新生成的文件添加目录索引
* 参照这里接下来的内容编写新生成的文件
* 使用命令：mkdocs serve，通过浏览本地服务：http://127.0.0.1:8000，来确认新编写的内容是正确无误的
* 将确认过的内容提交到本地Git仓库，并与GitLab仓库进行同步（同步前请先合并其他人的修改）
* 等待远端构建结束后，访问：https://bicf.gitlab.io/，做最终确认

## MarkDown语法

### 标题

使用`#`，表示1-6级标题。

```text
# 一级标题   
## 二级标题   
### 三级标题   
#### 四级标题   
##### 五级标题   
###### 六级标题    
```

注意:

- 一个.md里只能有一个 *#*，跟着多个 *##*。如果有多个 *#*，则不会自动生产本页目录。

- 如果有 *#*，则使用该标题作为本页正文部分第一行，如果没有#，则为mkdocs.yml里指定的pages名。

### 段落

一个段落表示一个语法块，每个语法块之间要有空行，若想在段内强制换行的方式是使用 **两个以上** 空格加上回车。

建议: 如果写一个语法，效果和想象的不一样，比如挤在一行内，或者没有缩进，则多1、2个换行试试

### 引用

在段落的每行或者只在第一行使用符号 *>* 如：

```text
> 区块引用
```

效果：
> 区块引用

另外引用还可以嵌套使用。

### 表格

**示例**

---

```text
| 字段1 | 字段2 | 字段3 |
| :-- | :--: | --: |
| A1 | B1 | C1 |
| A2 | B2 | C2 |
```

效果

| 字段1 | 字段2 | 字段3 |
| :-- | :--: | --: |
| A1 | B1 | C1 |
| A2 | B2 | C2 |

**详解**

---

至少由3行组成

- 第1行：字段名

	```text
	| 字段1 | 字段2 | 字段3 |
	```

- 第2行：设置对齐

	```text
	| :-- | :--: | --: |
	```

	分别表示为左对齐，居中对齐，又对齐。另外，也可以不设置对齐，即不写冒号，但这样不好维护，因此建议都设置对齐

- 第3行开始：正文

	```text
	| 内容1 | 内容2 | 内容3 |
	```

!!! warning "空格不可忽略"
	```text
	|字段1|字段2|字段3|
	```

	这样是有问题的，空格1个，也可以多个，但不能不写

!!! note "最左边和最右边的|可省略"
	```text
	| 字段1 | 字段2 | 字段3 |
	```

	也可以写成这样
	
	```text
	字段1 | 字段2 | 字段3
	```
	
	不仅是第1行，每一行都可以这么省略

### 字体样式

**斜体，粗体，粗斜体**

```text
*text* 或 _text_ 斜体

**text** 或 __text__ 粗体

***text*** 或 ___text___ 粗斜体
```

效果

*text* 斜体

**text** 粗体

***text*** 粗斜体

**上标，下标**

```text
10^2^
```

效果

10^2^

针对英文描述第几的上标，可以`1st`这种方式

| 用法                 | 效果              |
| :------------------ | :---------------- |
| `1st 2nd 3rd`       | 1st 2nd 3rd       |
| `1^st^ 2^nd^ 3^rd^` | 1^st^ 2^nd^ 3^rd^ |

```text
H~2~O
```

效果

H~2~O

**下划线，横线**

```text
^^underline me^^
```

效果

^^underline me^^

```text
This is ~~is~~ a test.
```

效果

This is ~~is~~ a test.

### 列表

**无序列表**

使用`*`、`+`、或`-`标记无序列表，下面仅以`-`作为示例，

```text
- 第一项
- 第二项
- 第三项
```

注: 标记后面最少有一个 *空格* 或 *制表符*。若不在引用区块中，必须和前方段落之间存在空行。

效果

- 第一项
- 第二项
- 第三项

**有序列表**

有序列表的标记方式是将上述的符号换成数字,并辅以`.`

```text
1. 第一项   
2. 第二项    
3. 第三项    
```

效果

1. 第一项
2. 第二项
3. 第三项

**任务列表**

用法: `- [ ]` 或 `- [x]`，其中`[ ]`表示不打勾，`[x]`表示打勾，`-`可以用`+`或`*`替代

```text
- [x] 任务1
- [x] 任务2
- [x] 任务3
- [ ] 任务4
    - [x] 任务4.1
    - [x] 任务4.2
    - [x] 任务4.3
    - [ ] 任务4.4
- [x] 任务5
- [ ] 任务6
```

效果

- [x] 任务1
- [x] 任务2
- [x] 任务3
- [ ] 任务4
    - [x] 任务4.1
    - [x] 任务4.2
    - [x] 任务4.3
    - [ ] 任务4.4
- [x] 任务5
- [ ] 任务6

### 分割线

分割线使用三个或以上`*`，也可以使用`-`和`_`，下面仅以`-`作为示例

```text
---
```

效果

---

### 链接

**普通链接-行内式**

```text
[example](http://www.example.com/ "title")
```

效果

[example](http://www.example.com/ "title")

鼠标悬停在链接上可以看到"title"字样

**普通链接-参考式**

当一个页面里多次调用相同链接时，这种方法更适用:

```
[example][索引]

...

[索引]: http://www.example.com/ "title"
```

注意: `[索引]: http://www.example.com/ "title"`可以写在任意地方，通常习惯于放在markdown本页文档最下方

效果

[example][索引]

[索引]: http://www.example.com/ "title"

鼠标悬停在链接上可以看到"title"字样

**自动链接**

当识别到HTML、FTP、Email地址时候会自动转为超链接，如

```text
http://www.example.cpm/
```

效果

http://www.example.com/

经测试，至少支持以下几种自动链接

```text
http://www.example.com/    
https://www.example.com/
ftp://www.example.com/
www.example.com
user@example.com
```

!!! warning "注意"
    例如PDF，PPT这样的附件，请先拷贝至docs目录下的attachments子目录中
	然后再在文档中添加链接以便他人下载

### 图片

首先，需要将图片拷贝到docs文件夹下的img子文件内。

**行内式**

添加图片的形式和链接相似，只需在链接的基础上前方加一个`!`

```text
![alt](图片地址 "title")
```

如

```text
![hello](./../img/logo.jpg "title")
```

效果

![hello](./../img/logo.jpg "title")

鼠标悬停在图标上可以看到"title"字样

**参考式**

当一个页面里多次调用相同图片时，这种方法更适用:

```text
![alt][索引]

[索引]: 图片地址 "title"
```

注意: `[索引]: 图片地址 "title"`可以写在任意地方，通常习惯于放在markdown本页文档最下方

如

```text
![alt][test icon]

...

[test icon]: ./../img/logo.jpg "title"
```

效果

![alt][test icon]

[test icon]: ./../img/logo.jpg "title"

鼠标悬停在图标上可以看到"title"字样

### 代码

**行内**

在一行内要标记的文本前后各加一个 *`*

```text
AAA`BBB`CCC
```

效果：

AAA`BBB`CCC

**区块**

在每行前面加上4个空格

```text
	text = "Hello, world!"
	print text
```

效果

	text = "Hello, world!"
	print text

或通过3个 `

	```text
	text = "Hello, world!"
	print text
	```

效果

```text
text = "Hello, world!"
print text
```

注: 如果代码区块上方是列表，那么4个空格不会被解析为代码区块，而是被解析为一个普通缩进

**行内高亮**

可以在一行文本里实现代码高亮

```text
`#!python print "Hello, world!"`或`:::python print "Hello, world!"`
```

效果

`#!python print "Hello, world!"`或`:::python print "Hello, world!"`

!!! warning "开头不能有空格，如果有空格的话，就会被认为是正文"
    ```text
    ` #!python print "Hello, world!"`
    ```
	效果
	` #!python print "Hello, world!"`

	```text
	` :::python print "Hello, world!"`
	```
	效果
	` :::python print "Hello, world!"`

**区块高亮**

写法1：3个\`

    ```python
    text = "Hello, world!"
    print text
    ```

效果

```python
text = "Hello, world!"
print text
```

写法2：4个空格+Shebang（由井号和叹号构成的字符序列）
```text
    #!/usr/bin/python
	text = "Hello, world!"
	print text
```

效果

	#!/usr/bin/python
	text = "Hello, world!"
	print text

共支持支持434种格式。若指定了不存在的格式，则等同于text，不做任何高亮渲染。

若不想使用高亮，用text，如：

    ```text
    text = "Hello, world!"
    print text
    ```

效果

```text
text = "Hello, world!"
print text
```

参数1：指定第几行背景高亮

    ```python hl_lines="2 4"
    text1 = "Hello, "
    text2 = "world!"
    print text1 + text2
    ```

效果

```python hl_lines="2 4"
text1 = "Hello, "
text2 = "world!"
print text1 + text2
```

参数2：指定行号从多少开始编号：

    ```python linenums="2"
    text1 = "Hello, "
    text2 = "world!"
    print text1 + text2
    ```

效果

```python linenums="2"
text1 = "Hello, "
text2 = "world!"
print text1 + text2
```

注意：代码块linenums参数不会影响hl_lines，即无论linenums指定从多少行开始编号，hl_lines实际都以行号从1开始编号来找到对应行进行背景高亮渲染，如：

    ```python linenums="2" hl_lines="3"
    text1 = "Hello, "
    text2 = "world!"
    print text1 + text2
    ```

效果

```python linenums="2" hl_lines="3"
text1 = "Hello, "
text2 = "world!"
print text1 + text2
```

### 数学公式

MarkDown包含对TeX数学公式的支持

行内写法有2种: `$...$` 和 `\(...\)`

区块写法有3种: `$$...$$`, `\[...\]`, 和 `\begin{}...\end{}`

具体用法详见:

- https://math.meta.stackexchange.com/questions/5020/mathjax-basic-tutorial-and-quick-reference

- http://www.andy-roberts.net/writing/latex/mathematics_1

- http://www.andy-roberts.net/writing/latex/mathematics_2

此处仅举个例子：

```text
$$
p(x|y) = \frac{p(y|x)p(x)}{p(y)}
$$
```

效果

$$
p(x|y) = \frac{p(y|x)p(x)}{p(y)}
$$

```text
$p(x|y) = \frac{p(y|x)p(x)}{p(y)}$
```

效果

$p(x|y) = \frac{p(y|x)p(x)}{p(y)}$

### 脚注

第一步. 插入引用

    Lorem ipsum[^1] dolor sit amet, consectetur adipiscing elit.[^2]

第二步. 插入内容

    [^1]: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
    [^2]:
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
        nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
        massa, nec semper lorem quam in massa.

!!! note ""
    可以看到，上面脚注1内容是单行方式，脚注2内容是多行方式

效果

Lorem ipsum[^1] dolor sit amet, consectetur adipiscing elit.[^2]

[^1]: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
[^2]:
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

!!! warning "注意"
    1. 不一定要用\[\^1\]，也可以用自定义关键字，比如\[\^mykey\]，但这样下面也要改为\[\^mykey\]，也可以支持空格，比如\[\^my key\]（当然后面也要\[\^my key\]:

    2. 无论脚注标签写的是什么（即\[\^1\]或者\[\^mykey\]，在展示脚注标签和展示脚注内容时候都是自动用数字从1开始累加标记，而不是自己写的内容）
    
    3. \[\^1\]: 脚注内容无论写在.md中的任何位置，在展示时候都是自动位于文档的最下方，并且会自动添加---这样一个横线

需滚动至页面底部查看效果

### 注解

```text
!!! note "标题"
    内容
```

效果

!!! note "标题"
    内容

```text
!!! note ""
    内容
```

效果

!!! note ""
    内容

```text
!!! note
    内容
```

效果

!!! note
    内容

注意: 无标题时类型首字母会被大写，如上面note被大写成Note

无类型，则默认为note

注意这个时候标题必须写英文，并且首字母会被自动转成大写:

```text
!!! title
    内容
```

相当于

```text
!!! note "Title"
    内容
```

效果

!!! title
    内容

折叠特别适用于FAQ，即标题里写问题，内容里写答案。此时标题不能为空，否则样式有问题

```text
??? note "标题"
    内容
```

效果

??? note "标题"
    内容

注意: 折叠中的内容在折叠未打开时候通过ctrl-f是搜索不出来的，但可以通过页面上方的Search栏里搜到

**11种颜色样式**

有11种样式，每一种样式都支持多种关键字，比如note可以用seealso替代，summary可以用tldr替代

```text
!!! note "note, seealso"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.
```

```text
!!! summary "summary, tldr"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.
```

```text
!!! info "info, todo"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.
```

```text
!!! tip "tip, hint, important"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.
```

```text
!!! success "success, check, done"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.
```

```text
!!! question "question, help, faq"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.
```

```text
!!! warning "warning, caution, attention"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.
```

```text
!!! failure "failure, fail, missing"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.
```

```text
!!! danger "danger, error"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.
```

```text
!!! bug "bug"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.
```

```text
!!! quote "quote, cite"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.
```

效果

!!! note "note, seealso"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.


!!! summary "summary, tldr"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.


!!! info "info, todo"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.


!!! tip "tip, hint, important"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.


!!! success "success, check, done"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor


!!! question "question, help, faq"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.


!!! warning "warning, caution, attention"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.


!!! failure "failure, fail, missing"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

!!! danger "danger, error"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.


!!! bug "bug"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

!!! quote "quote, cite"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.




