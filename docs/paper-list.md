disqus:

# 文献阅读列表

## 分享安排


###十一月主题 Representation

* 11月5日
* 11月12日
* 11月19日
* 11月26日

###十月主题 Dynamic

* 10月8日
* 10月15日
* 10月22日
* 10月29日

###九月主题 Reinforcement Learning

* 9月10日，许铁 - 不同RNN动力系统在导航中的应用
* 9月17日
* 9月24日

###六月主题：Learning and Local Circuit

* 6月25日，Circuit 
	1. Cortical circuits implement optimal context integration 
	2. Neural dynamics at successive stages of the ventral visual stream are consistent with hierarchical error signals
* 6月18日，Learning 
	1. Synaptic Plasticity Dynamics for Deep Continuous Local Learning；
	2. Meta-Learning paper
* 6月11日，Coding
	1. Exploring Randomly Wired Neural Networks for Image Recognition 
	2. A large-scale, standardized physiological survey reveals higher order coding throughout the mouse visual cortex; 
	3. Hyperbolic Attention Networks
* 6月4日，Deep Learning: A Critical Appraisal

###五月主题：Neural Symbolic/Graphical

* 5月28日，张文博/沈新科 semantics: 
	1. A unified model of human semantic knowledge and its disorders; 
	2. Modality-independent encoding of individual concepts in the left parietal cortex; 
	3. The neuro-cognitive representations of symbols: the case of concrete words
* 5月21日，庞浩/沈新科/渠鹏 rule learning: 
	1. Foundations of human reasoning in the prefrontal cortex
	2. Neural Computations Underlying Causal Structure Learning
	3. Task representations in neural networks trained to perform many cognitive tasks
* 5月14日，郑浩/唐李凯讲海马&learning相关的文献 
	1. The Dance of the Interneurons: How Inhibition Facilitates Fast Compressible and Reversible Learning in Hippocampus;
	2. SuperSpike: Supervised Learning in Multilayer Spiking Neural Networks 
	3. Synaptic Plasticity Dynamics for Deep Continuous Local Learning；
* 5月7日，张文博/李朋勇/刘祥根/闫宇坤讲Relational inductive biases, deep learning, and graph networks

###四月主题：强化学习与决策

* 4月30日，孙昱昊/郑浩，讲Reinforcement learning in artificial and biological systems和A distributed, hierarchical and recurrent framework for reward-based choice
* 4月23日，张文博/庞浩讲Evidence accumulation in a Laplace domain decision space和Integrating Models of Interval Timing and Reinforcement Learning
* 4月16日，刘祥根/庞浩/郑浩讲Wang XJ的前额叶皮层Meta-RL模型
* 4月9日，沈新科/孙昱昊讲Michael J Frank的几个模型
    1. general review of cannonical model
    2. decision making: prefrontal cortex, fMRI  
    3. rule learning: EEG
* 4月2日，庞浩讲Jane Wang的Meta-RL论文

## 重要链接

### 北大计算神经学Journal Club

[https://github.com/AmazingAng/PKU-Computational-Neuroscience-Journal-Club](https://github.com/AmazingAng/PKU-Computational-Neuroscience-Journal-Club)

### The Human Affectome Project

[http://www.neuroqualia.org/](http://www.neuroqualia.org/)

## 阅读安排

### 2019.11.10添加

* [Corticostriatal Flow of Action Selection Bias](https://www.sciencedirect.com/science/article/abs/pii/S0896627319308025 "") - 未添加

### 2019.11.9添加

* [Layer-Specific Contributions to Imagined and Executed Hand Movements in Human Primary Motor Cortex](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3482808 "") - 未添加
* [Specific Hippocampal Interneurons Shape Consolidation of Recognition Memory](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3482875 "") - 未添加

### 2019.11.6添加

* [Spatio-Temporal Backpropagation for Training High-Performance Spiking Neural Networks](https://www.frontiersin.org/articles/10.3389/fnins.2018.00331/full "") - 未添加

### 2019.11.5添加

* [The Infancy of the Human Brain](https://www.ncbi.nlm.nih.gov/pubmed/26447575 "") - 未添加

### 2019.11.4添加

* [A deep learning framework for neuroscience](https://www.nature.com/articles/s41593-019-0520-2 "") - 未添加

### 2019.10.29添加

* [Episodic memory: Neuronal codes for what, where, and when](https://onlinelibrary.wiley.com/doi/pdf/10.1002/hipo.23132 "") - 未添加
* [Shai Ben-David：无监督学习中的鲜花与荆棘](https://mp.weixin.qq.com/s?__biz=MzIzNjc0MTMwMA==&mid=2247492492&idx=1&sn=f3c03f1e04c0cd93aa685d5dcbc13723&chksm=e8d19457dfa61d4141067ce2a286a9a9e77c05e482630ed95083b5701785707cadef90652465&mpshare=1&scene=1&srcid=&sharer_sharetime=1572776270879&sharer_shareid=2a1d64052d54c36cfcf3ae266295a48a&key=443d50b7f259efee805ed3ee3de9d193303628eb70ca9adb4c1494b3ecc47f8c73f2c096edcc1908043ce0f157fc9ff8c25f34c2295f0cc48eb2d0ba2e64004c1eb1e499b51594a818767a2bab5e0705&ascene=1&uin=MjI4NTU2NjU2Mw%3D%3D&devicetype=Windows+10&version=62060833&lang=zh_CN&pass_ticket=LNI6qClNsFW1g%2F%2B6d7Jry56OfCUtgy3soatSWWEQjoIioKmdjET9fu5E9tth9hco "WeChat") - 未添加

### 2019.10.28添加

* [Hierarchical Encoding of Attended Auditory Objects in Multi-talker Speech Perception](https://www.cell.com/neuron/fulltext/S0896-6273(19)30780-9?_returnURL=https%3A%2F%2Flinkinghub.elsevier.com%2Fretrieve%2Fpii%2FS0896627319307809%3Fshowall%3Dtrue "") - 未添加
* [The neurobiology of language beyond single-word processing](https://science.sciencemag.org/content/366/6461/55.full "") - 未添加

### 2019.10.27添加

* [Neural Correlates of Optimal Multisensory Decision Making under Time-Varying Reliabilities with an Invariant Linear Probabilistic Population Code](https://www.sciencedirect.com/science/article/abs/pii/S0896627319307445 "") - 未添加
* [Reactivation, Replay, and Preplay: How It Might All Fit Together](https://www.hindawi.com/journals/np/2011/203462/ "") - 未添加
* [Generative Predictive Codes by Multiplexed Hippocampal Neuronal Tuplets](https://news.yale.edu/sites/default/files/files/Generative%20Predictive%20Codes%20by%20Multiplexed%20Hippocampal%20Neuronal%20Tuplets.pdf "") - 未添加
* [Theoretical Principles of Multiscale Spatiotemporal Control of Neuronal Networks: A Complex Systems Perspective](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6187923/ "") - 未添加
* [Mindfulness, Interoception, and the Body: A Contemporary Perspective](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6753170/ "") - 未添加
* [Psychoanalysis and Neuroscience: The Bridge Between Mind and Brain](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6724748/ "") - 未添加
* [Evaluation of the spatial variability in the major resting‐state networks across human brain functional atlases](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6771873/ "") - 未添加
* [Frontal cortex tracks surprise separately for different sensory modalities but engages a common inhibitory control mechanism](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6687204/ "") - 未添加
* [A graph representation of functional diversity of brain regions](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6749480/ "") - 未添加

### 2019.10.26添加

* [Cognitive chimera states in human brain networks](https://advances.sciencemag.org/content/5/4/eaau8535?fbclid=IwAR2SUZIWj2nEweT4Eqc6ocl6i-r1tJNTlapcOqisc_0JsGlTMIU16YgboIw&intcmp=trendmd-adv&_ga=2.237004992.1583656607.1571935789-114038881.1571241374 "") - 未添加
* [Integrative states within the brain](https://science.sciencemag.org/content/363/6429/832.6?intcmp=trendmd-sci "") - 未添加
* [Computational rationality: A converging paradigm for intelligence in brains, minds, and machines](https://science.sciencemag.org/content/349/6245/273?intcmp=collection-ai "") - 未添加
* [Multiplexed oscillations and phase rate coding in the basal forebrain](https://advances.sciencemag.org/content/4/8/eaar3230?intcmp=trendmd-adv&_ga=2.237004992.1583656607.1571935789-114038881.1571241374 "") - 未添加
* [Space and Time: The Hippocampus as a Sequence Generator](https://www.sciencedirect.com/science/article/abs/pii/S1364661318301669 "") - 未添加
* [Systema Temporis: A time-based dimensional framework for consciousness and cognition](https://www.sciencedirect.com/science/article/abs/pii/S1053810019301655 "") - 未添加

### 2019.10.25添加

* [Computational Architecture of the Parieto-Frontal Network Underlying Cognitive-Motor Control in Monkeys](https://www.eneuro.org/content/4/1/ENEURO.0306-16.2017 "") - 未添加
* [The Neural Representation of Sequences: From Transition Probabilities to Algebraic Patterns and Linguistic Trees](https://www.sciencedirect.com/science/article/pii/S089662731500776X "") - 未添加
* [Learning predictive structure without a teacher: decision strategies and brain routes](https://www.sciencedirect.com/science/article/abs/pii/S0959438818302393 "") - 未添加
* [The Dorsal Frontoparietal Network: A Core System for Emulated Action](https://www.cell.com/trends/cognitive-sciences/pdf/S1364-6613(17)30100-6.pdf "") - 未添加
* [Neural Organization of Hierarchical Motor Sequence Representations in the Human Neocortex](https://www.sciencedirect.com/science/article/abs/pii/S0896627319305677 "") - 未添加
* [The neurobiology of language beyond single-word processing](https://science.sciencemag.org/content/366/6461/55.full "") - 未添加
* [The neural basis of combinatory syntax and semantics](https://science.sciencemag.org/content/366/6461/62 "") - 未添加
* [Brain signatures of a multiscale process of sequence learning in humans](https://elifesciences.org/articles/41541 "") - 未添加
* [Event Representations and Predictive Processing: The Role of the Midline Default Network Core](https://onlinelibrary.wiley.com/doi/full/10.1111/tops.12450 "") - 未添加
* [Neural Markers of Event Boundaries](https://onlinelibrary.wiley.com/doi/10.1111/tops.12470 "") - 未添加
* [Brain dynamics and temporal trajectories during task and naturalistic processing](https://www.sciencedirect.com/science/article/abs/pii/S105381191832086X "") - 未添加

### 2019.10.23添加

* [Frontal Cortex and the Hierarchical Control of Behavior](./attachments/paper-list/x.pdf "") - 未添加
* [Multimodal gradients across mouse cortex](./attachments/paper-list/x.pdf "") - 未添加
* [Inhibitory Control: Mapping Medial Frontal Cortex](./attachments/paper-list/x.pdf "") - 未添加
* [A Review of Motivational Systems and Emotions in Cognitive Architectures and Systems](https://link.springer.com/chapter/10.1007/978-3-030-33274-7_4 "") - 未添加

### 2019.10.22添加

* [The Dorsal Frontoparietal Network: A Core System for Emulated Action](./attachments/paper-list/x.pdf "") - 未添加
* [Hippocampal sharp-wave ripples linked to visual episodic recollection in humans](./attachments/paper-list/x.pdf "") - 未添加
* [Mindfulness, Interoception, and the Body: A Contemporary Perspective](./attachments/paper-list/x.pdf "") - 未添加
* [An integrative computational architecture for object-driven cortex](./attachments/paper-list/x.pdf "") - 未添加
* [Backpropagation through time and the brain](./attachments/paper-list/x.pdf "") - 未添加
* [The algorithmic architecture of exploration in the human brain](./attachments/paper-list/x.pdf "") - 未添加
* [Computational mechanisms of curiosity and goal-directed exploration](./attachments/paper-list/x.pdf "") - 未添加

### 2019.10.21添加

* [The Distributed Nature of Working Memory](./attachments/paper-list/x.pdf "") - 未添加
* [Shaping Action Sequences in Basal Ganglia Circuits](./attachments/paper-list/x.pdf "") - 未添加
* [Discrete circuits support generalized versus context-specific vocal learning in the songbird](./attachments/paper-list/x.pdf "") - 未添加
* [Direct and indirect dorsolateral striatum pathways reinforce different action strategies](./attachments/paper-list/x.pdf "") - 未添加
* [Simple Plans or Sophisticated Habits? State, Transition and Learning Interactions in the Two-Step Task](./attachments/paper-list/x.pdf "") - 未添加
* [Basal Ganglia Subcircuits Distinctively Encode the Parsing and Concatenation of Action Sequences](./attachments/paper-list/x.pdf "") - 未添加
* [Concurrent Activation of Striatal Direct and Indirect Pathways During Action Initiation](./attachments/paper-list/x.pdf "") - 未添加
* [Resolving the Electroencephalographic Correlates of Rapid Goal-Directed Chunking in the Frontal-Parietal Network](./attachments/paper-list/x.pdf "") - 未添加
* [Distinct roles of striatal direct and indirect pathways in value-based decision making](./attachments/paper-list/x.pdf "") - 未添加
* [Interactive reservoir computing for chunking information streams](./attachments/paper-list/x.pdf "") - 未添加
* [Space and time: The hippocampus as a sequence generator](./attachments/paper-list/x.pdf "") - 未添加
* [Distinct Populations of Motor Thalamic Neurons Encode Action Initiation, Action Selection, and Movement Vigor](./attachments/paper-list/x.pdf "") - 未添加
* [Optogenetic editing reveals the hierarchical organization of learned action sequences](./attachments/paper-list/x.pdf "") - 未添加
* [Differential inputs to striatal cholinergic and parvalbumin interneurons imply functional distinctions](./attachments/paper-list/x.pdf "") - 未添加
* [Coordinated Ramping of Dorsal Striatal Pathways preceding Food Approach and Consumption](./attachments/paper-list/x.pdf "") - 未添加
* [A Basal Ganglia Circuit Sufficient to Guide Birdsong Learning](./attachments/paper-list/x.pdf "") - 未添加
* [Activation of striatal neurons causes a perceptual decision bias during visual change detection in mice](./attachments/paper-list/x.pdf "") - 未添加
* [Differential coding of reward and movement information in the dorsomedial striatal direct and indirect pathways](./attachments/paper-list/x.pdf "") - 未添加
* [Regulation of habit formation in the dorsal striatum](./attachments/paper-list/x.pdf "") - 未添加
* [Striatal local circuitry: a new framework for lateral inhibition](./attachments/paper-list/x.pdf "") - 未添加
* [The Spatiotemporal Organization of the Striatum Encodes Action Space](./attachments/paper-list/x.pdf "") - 未添加
* [A Pause-then-Cancel model of stopping: evidence from basal ganglia neurophysiology](./attachments/paper-list/x.pdf "") - 未添加
* [Reinforcement learning with Marr](./attachments/paper-list/x.pdf "") - 未添加

### 2019.10.20添加

* [Edge of chaos and scale-free avalanches in neural networks with heavy-tailed synaptic disorder](./attachments/paper-list/x.pdf "") - 未添加

### 2019.10.17添加

* [FlexLearn: Fast and Highly Efficient Brain Simulations Using Flexible On-Chip Learning](./attachments/paper-list/x.pdf "") - 未添加
* [Hippocampal sharp-wave ripples linked to visual episodic recollection in humans](./attachments/paper-list/x.pdf "") - 未添加
* [Generative Predictive Codes by Multiplexed Hippocampal Neuronal Tuplets](./attachments/paper-list/x.pdf "") - 未添加
* [Saliency, switching, attention and control: a network model of insula function](./attachments/paper-list/x.pdf "") - 未添加
* [Evaluation of the spatial variability in the major resting‐state networks across human brain functional atlases](./attachments/paper-list/x.pdf "") - 未添加
* [Psychoanalysis and Neuroscience: The Bridge Between Mind and Brain](./attachments/paper-list/x.pdf "") - 未添加
* [A blueprint of mammalian cortical connectomes](./attachments/paper-list/x.pdf "") - 未添加
* [The brain's connective core and its role in animal cognition](./attachments/paper-list/x.pdf "") - 未添加
* [What is consciousness, and could machines have it?](./attachments/paper-list/x.pdf "") - 未添加

### 2019.10.6添加

* [The neural substrate of self- and other-concerned wellbeing: An fMRI study](./attachments/paper-list/x.pdf "") - 未添加

### 2019.10.5添加

* [Kinase and Phosphatase Engagement Is Dissociated Between Memory Formation and Extinction](./attachments/paper-list/x.pdf "") - 未添加

### 2019.10.4添加

* [Brain Computation: 2 A Computer Science Perspective](./attachments/paper-list/x.pdf "") - 未添加

### 2019.9.30添加

* [Gate Decorator: Global Filter Pruning Method for Accelerating Deep Convolutional Neural Networks](./attachments/paper-list/x.pdf "") - 未添加
* [InterpretML: A Unified Framework for Machine Learning Interpretability](./attachments/paper-list/x.pdf "") - 未添加
* [ALBERT: A LITE BERT FOR SELF-SUPERVISED LEARNING OF LANGUAGE REPRESENTATIONS](./attachments/paper-list/x.pdf "") - 未添加
* [Language Models as Knowledge Bases?](./attachments/paper-list/x.pdf "") - 未添加
* [Deep Learning For Symbolic Mathematics](./attachments/paper-list/x.pdf "") - 未添加
* [Quantum Supremacy Using a Programmable Superconducting Processor](./attachments/paper-list/x.pdf "") - 未添加
* [Do Massively Pretrained Language Models Make Better Storytellers?](./attachments/paper-list/x.pdf "") - 未添加
* [TinyBERT: Distilling BERT for Natural Language Understanding](./attachments/paper-list/x.pdf "") - 未添加
* [Navigating Social Space](./attachments/paper-list/x.pdf "") - 未添加
* [Neural Circuit Motifs in Valence Processing](./attachments/paper-list/x.pdf "") - 未添加
* [What Is a Cognitive Map? Organizing Knowledge for Flexible Behavior](./attachments/paper-list/x.pdf "") - 未添加
* [Dissecting the Synapse- and Frequency-Dependent Network Mechanisms of In Vivo Hippocampal Sharp Wave-Ripples](./attachments/paper-list/x.pdf "") - 未添加
* [Large-Scale Cortical Networks for Hierarchical Prediction and Prediction Error in the Primate Brain](./attachments/paper-list/x.pdf "") - 未添加
* [Is there a prediction network? Meta-analytic evidence for a cortical-subcortical network likely subserving prediction](./attachments/paper-list/x.pdf "") - 未添加
* [Theories of rhythmogenesis](./attachments/paper-list/x.pdf "") - 未添加
* [Learning predictive structure without a teacher: decision strategies and brain routes](./attachments/paper-list/x.pdf "") - 未添加
* [An argument for hyperbolic geometry in neural circuits](./attachments/paper-list/x.pdf "") - 未添加
* [Classes of dendritic information processing](./attachments/paper-list/x.pdf "") - 未添加
* [Cortical computations via metastable activity](./attachments/paper-list/x.pdf "") - 未添加
* [Engagement of Pulvino-cortical Feedforward and Feedback Pathways in Cognitive Computations](./attachments/paper-list/x.pdf "") - 未添加
* [The Puzzling Pulvinar](./attachments/paper-list/x.pdf "") - 未添加
* [Relating network connectivity to dynamics: opportunities and challenges for theoretical neuroscience](./attachments/paper-list/x.pdf "") - 未添加
* [Big-Loop Recurrence within the Hippocampal System Supports Integration of Information across Episodes](./attachments/paper-list/x.pdf "") - 未添加

### 2019.9.28添加

* [Forgetting at biologically realistic levels of neurogenesis in a large-scale hippocampal model](./attachments/paper-list/10.1016@j.bbr.2019.112180.pdf "Behavioural Brain Research") - 未添加
* [Intersectional monosynaptic tracing for dissecting subtype-specific organization of GABAergic interneuron inputs](./attachments/paper-list/10.1038@s41593-018-0322-y.pdf "nature neuroscience") - 未添加
* [Where Is the Semantic System? A Critical Review and Meta-Analysis of 120 Functional Neuroimaging Studies](./attachments/paper-list/10.1093@cercor@bhp055.pdf "Cereb Cortex") - 未添加
* [Cyclic transitions between higher order motifs underlie sustained activity in asynchronous sparse recurrent networks](./attachments/paper-list/777219.full.pdf "bioRxiv") - 未添加
* [A Hippocampus-Accumbens Tripartite Neuronal Motif Guides Appetitive Memory in Space](./attachments/paper-list/10.1016@j.cell.2018.12.037.pdf "cell") - 未添加
* [Multiplexing of Theta and Alpha Rhythms in the Amygdala-Hippocampal Circuit Supports Pattern Separation of Emotional Information](./attachments/paper-list/10.1016@j.neuron.2019.03.025.pdf "neuron") - 未添加
* [VIP Interneurons Contribute to Avoidance Behavior by Regulating Information Flow across Hippocampal-Prefrontal Networks](./attachments/paper-list/10.1016@j.neuron.2019.04.001.pdf "neuron") - 未添加
* [Central Amygdala Prepronociceptin-Expressing Neurons Mediate Palatable Food Consumption and Reward](./attachments/paper-list/10.1016@j.neuron.2019.03.037.pdf "neuron") - 未添加
* [A Common Neuroendocrine Substrate for Diverse General Anesthetics and Sleep](./attachments/paper-list/10.1016@j.neuron.2019.03.033.pdf "neuron") - 未添加
* [The Spatial Structure of Neural Encoding in Mouse Posterior Cortex during Navigation](./attachments/paper-list/10.1016@j.neuron.2019.01.029.pdf "neuron") - 未添加
* [Somatostatin-Expressing Interneurons Enable and Maintain Learning-Dependent Sequential Activation of Pyramidal Neurons](./attachments/paper-list/10.1016@j.neuron.2019.01.036.pdf "neuron") - 未添加
* [Cortical Areas Interact through a Communication Subspace](./attachments/paper-list/10.1016@j.neuron.2019.01.026.pdf "neuron") - 未添加
* [Hierarchical Heterogeneity across Human Cortex Shapes Large-Scale Neural Dynamics](./attachments/paper-list/10.1016@j.neuron.2019.01.017.pdf "neuron") - 未添加
* [Hippocampal Contributions to Model-Based Planning and Spatial Memory](./attachments/paper-list/10.1016@j.neuron.2019.02.014.pdf "neuron") - 未添加
* [A Network for Computing Value Equilibrium in the Human Medial Prefrontal Cortex](./attachments/paper-list/10.1016@j.neuron.2018.12.029.pdf "neuron") - 未添加
* [Layers of Rhythms — from Cortical Anatomy to Dynamics](./attachments/paper-list/10.1016@j.neuron.2019.01.028.pdf "neuron") - 未添加
* [Nonoscillatory Phase Coding and Synchronization in the Bat Hippocampal Formation](./attachments/paper-list/10.1016@j.cell.2018.09.017.pdf "neuron") - 未添加
* [Conducting the Neural Symphony of Memory Replay](./attachments/paper-list/10.1016@j.neuron.2018.11.037.pdf "neuron") - 未添加
* [Dissecting the Synapse- and Frequency-Dependent Network Mechanisms of In Vivo Hippocampal Sharp Wave-Ripples](./attachments/paper-list/10.1016@j.neuron.2018.09.041.pdf "neuron") - 未添加
* [Spike Time Synchrony in the Absence of Continuous Oscillations](./attachments/paper-list/10.1016@j.neuron.2018.10.036.pdf "neuron") - 未添加
* [The Theta Rhythm Mixes and Matches Gamma Oscillations Cycle by Cycle](./attachments/paper-list/10.1016@j.neuron.2018.11.008.pdf "neuron") - 未添加
* [Robots that Imagine – Can Hippocampal Replay Be Utilized for Robotic Mnemonics?](./attachments/paper-list/10.1007@978-3-030-24741-6_24.pdf "Biomimetic and Biohybrid Systems") - 未添加

### 2019.9.25添加

* [Large-scale cortical correlation structure of spontaneous oscillatory activity](./attachments/paper-list/10.1038@nn.3101.pdf "nature neuroscience") - 未添加
* [Dynamic reconfiguration of cortical functional connectivity across brain states](./attachments/paper-list/10.1038@s41598-017-08050-6.pdf "scientific reports") - 未添加

### 2019.9.18添加

* [Layer 4 of mouse neocortex differs in cell types and circuit organization between sensory areas](./attachments/paper-list/10.1038@s41467-019-12058-z.pdf "nature communications") - 未添加

### 2019.9.14添加

* [Persistence of neuronal representations through time and damage in the hippocampus](./attachments/paper-list/10.1126@science.aav9199.pdf "Science") - 未添加

### 2019.9.12添加

* [VideoBERT: A Joint Model for Video and Language Representation Learning](./attachments/paper-list/1904.01766.pdf "arxiv") - 待定
* [Contrastive Bidirectional Transformer for Temporal Representation Learning](./attachments/paper-list/1906.05743.pdf "arxiv") - 待定
* [CTRL: A CONDITIONAL TRANSFORMER LANGUAGE MODEL FOR CONTROLLABLE GENERATION](./attachments/paper-list/ctrl.pdf "CTRL2019") - 待定

### 2019.9.7添加

* [Human Replay Spontaneously Reorganizes Experience](./attachments/paper-list/10.1016@j.cell.2019.06.012.pdf "cell") - 待定

### 2019.9.4添加

* [Experimental analyses on 2-hop-based and 3-hop-based link prediction algorithms](./attachments/paper-list/1909.00174.pdf "arxiv") - 待定
* [A solution to the learning dilemma for recurrent networks of spiking neurons](./attachments/paper-list/738385.full.pdf "bioRxiv") - 待定

### 2019.9.2添加

* [What is the dynamical regime of cerebral cortex?](./attachments/paper-list/1908.10101.pdf "arxiv") - 待定
* [The MK2 cascade regulates mGluR-dependent synaptic plasticity and reversal learning](./attachments/paper-list/10.1016@j.neuropharm.2019.05.024.pdf "Neuropharmacology") - 待定
* [PKCα integrates spatiotemporally distinct Ca2+ and autocrine BDNF signaling to facilitate synaptic plasticity](./attachments/paper-list/10.1038@s41593-018-0184-3.pdf "nature neuroscience") - 待定
* [Rac1 is a downstream effector of PKCα in structural synaptic plasticity](./attachments/paper-list/750372.full.pdf "bioRxiv") - 待定

### 2019.9.1添加

* [Constraining computational models using electron microscopy wiring diagrams](./attachments/paper-list/10.1016@j.conb.2019.07.007.pdf "Current Opinion in Neurobiology") - 待定
* [Structured connectivity exploits NMDA non-linearities to enable flexible encoding of multiple memoranda in a PFC circuit model](./attachments/paper-list/733519.full.pdf "bioRxiv") - 待定
* [STDP Forms Associations between Memory Traces in Networks of Spiking Neurons](./attachments/paper-list/10.1093@cercor@bhz140.pdf "Cerebral Cortex") - 待定
* [The neural and cognitive architecture for learning from a small sample](./attachments/paper-list/10.1016@j.conb.2019.02.011.pdf "Current Opinion in Neurobiology") - 待定
* [The algorithmic architecture of exploration in the human brain](./attachments/paper-list/10.1016@j.conb.2018.11.003.pdf "Current Opinion in Neurobiology") - 待定
* [What does the mind learn? A comparison of human and machine learning representations](./attachments/paper-list/10.1016@j.conb.2019.02.004.pdf "Current Opinion in Neurobiology") - 待定
* [An integrative computational architecture for object-driven cortex](./attachments/paper-list/10.1016@j.conb.2019.01.010.pdf "Current Opinion in Neurobiology") - 待定
* [Machine learning and big data in psychiatry: toward clinical applications](./attachments/paper-list/10.1016@j.conb.2019.02.006.pdf "Current Opinion in Neurobiology") - 待定

### 2019.8.30添加

* [Consciousness: The last 50 years (and the next)](./attachments/paper-list/10.1177@2398212818816019.pdf "Brain and Neuroscience Advances") - 待定

### 2019.8.28添加

* [A Topology Layer for Machine Learning](./attachments/paper-list/1905.12200.pdf "arxiv") - 待定
* [DRAW: A Recurrent Neural Network For Image Generation](./attachments/paper-list/1502.04623.pdf "arxiv") - 待定

### 2019.8.26添加

* [Adaptive Attention Span in Transformers](./attachments/paper-list/1905.07799.pdf "arxiv") - 待定
* [Augmenting Self-attention with Persistent Memory](./attachments/paper-list/1907.01470.pdf "arxiv") - 待定
* [Expectation-Maximization Attention Networks for Semantic Segmentation](./attachments/paper-list/1907.13426.pdf "arxiv") - 待定
* [Language Models are Unsupervised Multitask Learners](./attachments/paper-list/language_models_are_unsupervised_multitask_learners.pdf "openai") - 待定

### 2019.8.25添加

* [AutoGAN: Neural Architecture Search for Generative Adversarial Networks](./attachments/paper-list/1908.03835.pdf "arxiv") - 待定

### 2019.8.24添加

* [Coupled ripple oscillations between the medial temporal lobe and neocortex retrieve human memory](./attachments/paper-list/10.1126@science.aau8956.pdf "science") - 待定
* [Long-duration hippocampal sharp wave ripples improve memory](./attachments/paper-list/10.1126@science.aax0758.pdf "science") - 待定
* [Hippocampal sharp-wave ripples linked to visual episodic recollection in humans](./attachments/paper-list/10.1126@science.aax1030.pdf "science") - 待定

### 2019.8.23添加

* [Biological learning curves outperform existing ones in artificial intelligence algorithms](./attachments/paper-list/10.1038@s41598-019-48016-4.pdf "nature") - 待定

### 2019.8.22添加

* [Conserved cell types with divergent features in human versus mouse cortex](./attachments/paper-list/10.1038@s41586-019-1506-7.pdf "nature") - 待定
* [Attention, please! A Critical Review of Neural Attention Models in Natural Language Processing](./attachments/paper-list/1902.02181.pdf "arxiv") - 待定
* [Predicting the Future with Multi-scale Successor Representations](./attachments/paper-list/449470.full.pdf "bioRxiv") - 待定
* [The Successor Representation: Its Computational Logic and Neural Substrates](./attachments/paper-list/10.1523@JNEUROSCI.0151-18.2018.pdf "The Journal of Neuroscience") - 待定

### 2019.8.19添加

* [EIDETIC 3D LSTM: A MODEL FOR VIDEO PREDICTION AND BEYOND](./attachments/paper-list/bd20c943b4cf216371e5f710b113c2292ff0e7bb.pdf "ICLR 2019") - 待定
* [Accelerated CNN Training Through Gradient Approximation](./attachments/paper-list/1908.05460.pdf "arxiv") - 待定
* [RTHybrid: A Standardized and Open-Source Real-Time Software Model Library for Experimental Neuroscience](./attachments/paper-list/10.3389@fninf.2019.00011.pdf "Front. Neuroinform") - 待定

### 2019.8.18添加

* [Hierarchical clusters in neuronal populations with plasticity](./attachments/paper-list/1908.04103.pdf "arxiv") - 待定
* [Is the brain relativistic?](./attachments/paper-list/1908.04290.pdf "arxiv") - 待定

### 2019.8.17添加

* [Bayesian Inference for Large Scale Image Classification](./attachments/paper-list/1908.03491.pdf "arxiv") - 待定

### 2019.8.16添加

* [THE HSIC BOTTLENECK: DEEP LEARNING WITHOUT BACK-PROPAGATION](./attachments/paper-list/1908.01580.pdf "arxiv") - 待定
* [Cognitive computational neuroscience](./attachments/paper-list/10.1038@s41593-018-0210-5.pdf "NaTure NeurosCIenCe") - 待定

### 2019.8.14添加

* [The Generation of Time in the Hippocampal Memory System](./attachments/paper-list/10.1016@j.celrep.2019.07.042.pdf "Cell Reports") - 待定
* [Memory editing from science fiction to clinical practice](./attachments/paper-list/10.1038@s41586-019-1433-7.pdf "review articles") - 待定

### 2019.8.13添加

* [The Hippocampus and Neocortical Inhibitory Engrams Protect against Memory Interference](./attachments/paper-list/10.1016@j.neuron.2018.11.042.pdf "neuron") - 待定

### 2019.8.5添加

* [Look Again at the Syntax: Relational Graph Convolutional Network for Gendered Ambiguous Pronoun Resolution](./attachments/paper-list/1905.08868.pdf "arxiv") - 待定
* [How do the brain’s time and space mediate consciousness and its different dimensions? Temporo-spatial theory of consciousness (TTC)](./attachments/paper-list/10.1016@j.neubiorev.2017.07.013.pdf "Neuroscience and Biobehavioral Reviews") - 待定
* [Is temporo-spatial dynamics the “common currency” of brain and mind? In Quest of “Spatiotemporal Neuroscience”](./attachments/paper-list/10.1016@j.plrev.2019.05.002.pdf "Physics of Life Reviews") - 待定
* [Mathematics and the brain –a category theoretic approach to go beyond the neural correlates of consciousness](./attachments/paper-list/674242.full.pdf "bioRxiv") - 待定
* [Discrete Sequential Information Coding: Heteroclinic Cognitive Dynamics](./attachments/paper-list/10.3389@fncom.2018.00073.pdf "Front Comput Neurosci") - 待定
* [Assembly pointers for variable binding in networks of spiking neurons](./attachments/paper-list/Assembly_pointers_for_variable_binding_in_networks.pdf "researchgate") - 待定

### 2019.7.30添加

* [scGen predicts single-cell perturbation responses](./attachments/paper-list/10.1038@s41592-019-0494-8.pdf "nature methods") - 待定

### 2019.7.22添加

* [Relating network connectivity to dynamics: opportunities and challenges for theoretical neuroscience](./attachments/paper-list/10.1016@j.conb.2019.06.003.pdf "Current Opinion in Neurobiology") - 待定
* [Bridging large-scale neuronal recordings and large-scale network models using dimensionality reduction](./attachments/paper-list/10.1016@j.conb.2018.12.009.pdf "Current Opinion in Neurobiology") - 待定
* [Designing neural networks through neuroevolution](./attachments/paper-list/10.1038@s42256-018-0006-z.pdf "nature machine intelligence") - 待定
* [Predictive models avoid excessive reductionism in cognitive neuroimaging](./attachments/paper-list/10.1016@j.conb.2018.11.002.pdf "Current Opinion in Neurobiology") - 待定
* [Analyzing biological and artificial neural networks: challenges with opportunities for synergy?](./attachments/paper-list/10.1016@j.conb.2019.01.007.pdf "Current Opinion in Neurobiology") - 待定

### 2019.7.15添加

* [The Brainstem in Emotion: A Review](./attachments/paper-list/10.3389@fnana.2017.00015.pdf "Front Neuroanat") - 待定

### 2019.7.14添加

* [Peeling the Onion of Brain Representations](./attachments/paper-list/10.1146@annurev-neuro-080317-061906.pdf "Annual Review of Neuroscience") - 待定
* [The Theory and Neuroscience of Cerebellar Cognition](./attachments/paper-list/10.1146@annurev-neuro-070918-050258.pdf "Annual Review of Neuroscience") - 待定

### 2019.7.8添加

* [COMET: Commonsense Transformers for Automatic Knowledge Graph Construction](./attachments/paper-list/1906.05317.pdf "arxiv") - 待定

### 2019.7.3添加

* [The Sparse Manifold Transform](./attachments/paper-list/1806.08887.pdf "arxiv") - 待定
* [Regulation of adenylyl cyclase 5 in striatal neurons confers the ability to detect coincident neuromodulatory signals](./attachments/paper-list/597096.full.pdf "bioRxiv") - 待定
* [Precise Coordination of 3-dimensional Rotational Kinematics by Ventral Tegmental Area GABAergic Neurons](./attachments/paper-list/678391.full.pdf "bioRxiv") - 待定
* [To find better neural network models of human vision, find better neural network models of primate vision](./attachments/paper-list/688390.full.pdf "bioRxiv") - 待定
* [A large single-participant fMRI dataset for probing brain responses to naturalistic stimuli in space and time](./attachments/paper-list/687681.full.pdf "bioRxiv") - 待定
* [Neuronal responses in posterior parietal cortex during learning of implied serial order](./attachments/paper-list/689133.full.pdf "bioRxiv") - 待定
* [Exploring Perceptual Illusions in Deep Neural Networks](./attachments/paper-list/687905.full.pdf "bioRxiv") - 待定
* [Event-based pattern detection in active dendrites](./attachments/paper-list/690792.full.pdf "bioRxiv") - 待定
* [Interleaved training prevents catastrophic forgetting in spiking neural networks](./attachments/paper-list/688622.full.pdf "bioRxiv") - 待定
* [Critical brain dynamics and human intelligence](./attachments/paper-list/688655.full.pdf "bioRxiv") - 待定
* [NINscope: a versatile miniscope for multi-region circuit investigations](./attachments/paper-list/685909.full.pdf "bioRxiv") - 待定
* [Experience shapes activity dynamics and stimulus coding of VIP inhibitory and excitatory cells](./attachments/paper-list/686063.full.pdf "bioRxiv") - 待定
* [Quantitative Personality Predictions from a Brief EEG Recording](./attachments/paper-list/686907.full.pdf "bioRxiv") - 待定
* [Functional clusters of neurons in layer 6 of macaque V1](./attachments/paper-list/685990.full.pdf "bioRxiv") - 待定
* [Functional dissociation of theta oscillations in the frontal and visual cortices and their long-range network during sustained attention](./attachments/paper-list/684829.full.pdf "bioRxiv") - 待定
* [Controllability Governs the Balance Between Pavlovian and Instrumental Action Selection](./attachments/paper-list/596577.full.pdf "bioRxiv") - 待定
* [Brain functional connectivity dynamics in the aftermaths of affective and cognitive events](./attachments/paper-list/685396.full.pdf "bioRxiv") - 待定
* [Noradrenergic but not dopaminergic neurons signal task state changes and predict re-engagement after a failure](./attachments/paper-list/686428.full.pdf "bioRxiv") - 待定
* [Unsupervised decoding of single-trial EEG reveals unique states of functional brain connectivity that drive rapid speech categorization decisions](./attachments/paper-list/686048.full.pdf "bioRxiv") - 待定
* [The neural correlates of well-being: A systematic review of the human neuroimaging and neuropsychological literature](./attachments/paper-list/10.3758@s13415-019-00720-4.pdf "Behavioral Neuroscience") - 待定
* [What is animal happiness?](./attachments/paper-list/10.1111@nyas.13983.pdf "ANNALS OF THE NEW YORK ACADEMY OF SCIENCES") - 待定
* [Brain functional connectivity correlates of coping styles](./attachments/paper-list/10.3758@s13415-018-0583-7.pdf "Behavioral Neuroscience") - 待定
* [The hippocampal engram maps experience but not place](./attachments/paper-list/10.1126@science.aat5397.pdf "science") - 待定

### 2019.6.25添加

* [Circuit mechanisms for the maintenance and manipulation of information in working memory](./attachments/paper-list/10.1038@s41593-019-0414-3.pdf "nature neuroscience") - 待定
* [The brain's default mode network](./attachments/paper-list/10.1146@annurev-neuro-071013-014030.pdf "Annu Rev Neurosci") - 待定

### 2019.6.22添加

* [Developing a brain atlas through deep learning](./attachments/paper-list/10.1038@s42256-019-0058-8.pdf "nature machine intelligence") - 待定

### 2019.6.21添加

* [Noninvasive neuroimaging enhances continuous neural tracking for robotic device control](./attachments/paper-list/10.1126@scirobotics.aaw6844.pdf "HUMAN-ROBOT INTERACTION") - 待定

### 2019.6.20添加

* [Stacked Capsule Autoencoders](./attachments/paper-list/1906.06818.pdf "arxiv") - 待定

### 2019.6.18添加

* [Context-aware Embedding for Targeted Aspect-based Sentiment Analysis](./attachments/paper-list/Context-aware-Embedding-for-Targeted-Aspect-based-Sentiment-Analysis.pdf "ACL 2019") - 待定
* [Classification of electrophysiological and morphological neuron types in the mouse visual cortex](./attachments/paper-list/10.1038@s41593-019-0417-0.pdf "nature neuroscience") - 待定
* [Generalized leaky integrate-and-fire models classify multiple neuron types](./attachments/paper-list/10.1038@s41467-017-02717-4.pdf "nature communications") - 待定
* [Systematic generation of biophysically detailed models for diverse cortical neuron types](./attachments/paper-list/10.1038@s41467-017-02718-3.pdf "nature communications") - 待定
* [Integrating the Allen Brain Institute Cell Types Database into Automated Neuroscience Workflow](./attachments/paper-list/10.1007@s12021-017-9337-x.pdf "Neuroinformatics") - 待定
* [Transcriptomic correlates of neuron electrophysiological diversity](./attachments/paper-list/10.1371@journal.pcbi.1005814.pdf "PLOS") - 待定
* [On the potential for open-endedness in neural networks](./attachments/paper-list/1812.04907.pdf "arxiv") - 待定

### 2019.6.17添加

* [CNM: An Interpretable Complex-valued Network for Matching](./attachments/paper-list/1904.05298.pdf "arxiv") - 待定

### 2019.6.15添加

* [Functional Relevance of Different Basal Ganglia Pathways Investigated in a Spiking Model with Reward Dependent Plasticity](./attachments/paper-list/10.3389@fncir.2016.00053.pdf "Front Neural Circuits") - 待定
* [Evidence for a task-dependent switch in subthalamo-nigral basal ganglia signaling](./attachments/paper-list/10.1038@s41467-017-01023-3.pdf "Nat Commun") - 待定
* [The Connectivity Fingerprint of the Human Frontal Cortex, Subthalamic Nucleus, and Striatum](./attachments/paper-list/10.3389@fnana.2018.00060.pdf "Front Neuroanat") - 待定
* [Conditional routing of information to the cortex: a model of the basal ganglia's role in cognitive coordination](./attachments/paper-list/10.1037@a0019077.pdf "Psychol Rev") - 待定

### 2019.6.13添加

* [Editorial: The Janus Face of Language: Where Are the Emotions in Words and Where Are the Words in Emotions?](./attachments/paper-list/10.3389@fpsyg.2018.00650.pdf "Frontiers in Psychology") - 待定
* [Emotional words can be embodied or disembodied: the role of superficial vs. deep types of processing](./attachments/paper-list/10.3389@fpsyg.2015.00975.pdf "Frontiers in Psychology") - 待定
* [Representation Tradeoffs for Hyperbolic Embeddings](./attachments/paper-list/1804.03329.pdf "arxiv") - 待定
* [Multi-relational Poincaré Graph Embeddings](./attachments/paper-list/1905.09791.pdf "arxiv") - 待定
* [Discrete and Continuous Cell Identities of the Adult Murine Striatum](./attachments/paper-list/591396.full.pdf "bioRxiv") - 待定
* [Central Amygdala Prepronociceptin-Expressing Neurons Mediate Palatable Food Consumption and Reward](./attachments/paper-list/10.1016@j.neuron.2019.03.037.pdf "Neuron") - 待定
* [Specialized coding of sensory, motor and cognitive variables in VTA dopamine neurons](./attachments/paper-list/10.1038@s41586-019-1261-9.pdf "nature") - 待定
* [Striatal circuits for reward learning and decision-making](./attachments/paper-list/10.1038@s41583-019-0189-2.pdf "nature reviews neuroscience") - 待定
* [Weight Agnostic Neural Networks](./attachments/paper-list/1906.04358.pdf "arxiv") - 待定
* [Nucleus accumbens D2R cells signal prior outcomes and control risky decision-making](./attachments/paper-list/10.1038@nature17400.pdf "nature") - 待定
* [Multiple Regions of a Cortical Network Commonly Encode the Meaning of Words in Multiple Grammatical Positions of Read Sentences](./attachments/paper-list/10.1093@cercor@bhy110.pdf "Cerebral Cortex") - 待定
* [A cross-disorder connectome landscape of brain dysconnectivity](./attachments/paper-list/10.1038@s41583-019-0177-6.pdf "neuroscience") - 待定
* [Spontaneous behaviors drive multidimensional, brainwide activity](./attachments/paper-list/10.1126@science.aav7893.pdf "science") - 待定
* [Learning and attention reveal a general relationship between population activity and behavior](./attachments/paper-list/10.1126@science.aao0284.pdf "science") - 待定

### 2019.6.12添加

* [Challenging Common Assumptions in the Unsupervised Learning of Disentangled Representations](./attachments/paper-list/locatello19a.pdf "ICML 2019") - 待定
* [Rates of Convergence for Sparse Variational Gaussian Process Regression](./attachments/paper-list/1903.03571.pdf "ICML 2019") - 待定
* [Analogies Explained: Towards Understanding Word Embeddings](./attachments/paper-list/1901.09813.pdf "ICML 2019") - 待定
* [SATNet: Bridging deep learning and logical reasoning using a differentiable satisfiability solver](./attachments/paper-list/1905.12149.pdf "ICML 2019") - 待定
* [A Tail-Index Analysis of Stochastic Gradient Noise in Deep Neural Networks](./attachments/paper-list/1901.06053.pdf "ICML 2019") - 待定
* [Towards A Unified Analysis of Random Fourier Features](./attachments/paper-list/1806.09178.pdf "ICML 2019") - 待定
* [Amortized Monte Carlo Integration](./attachments/paper-list/UDL-12.pdf "ICML 2019") - 待定
* [Social Influence as Intrinsic Motivation for Multi-Agent Deep Reinforcement Learning](./attachments/paper-list/1810.08647.pdf "ICML 2019") - 待定
* [Stochastic Beams and Where to Find Them: The Gumbel-Top-k Trick for Sampling Sequences Without Replacement](./attachments/paper-list/1903.06059.pdf "ICML 2019") - 待定
* [Born to learn: The inspiration, progress, and future of evolved plastic artificial neural networks](./attachments/paper-list/10.1016@j.neunet.2018.07.013.pdf "Neural Networks") - 待定
* [Scalable analysis of cell-type composition from single-cell transcriptomics using deep recurrent learning](./attachments/paper-list/10.1038@s41592-019-0353-7.pdf "methods") - 待定
* [A null model of the mouse whole-neocortex micro-connectome](./attachments/paper-list/548735.full.pdf "biorxiv") - 待定
* [The Posterior Parietal Cortex in Adaptive Visual Processing](./attachments/paper-list/10.1016@j.tins.2018.07.012.pdf "Trends in Neurosciences") - 待定
* [An Integrative Framework for Sensory, Motor, and Cognitive Functions of the Posterior Parietal Cortex](./attachments/paper-list/10.1016@j.neuron.2018.01.044.pdf "Neuron Perspective") - 待定
* [The temporal signature of self: Temporal measures of resting-state EEG predict self-consciousness](./attachments/paper-list/10.1002@hbm.24412.pdf "Hum Brain Mapp") - 待定

### 2019.6.11添加

* [The Manifold of Life: the Math Subtending Living BEINGS’ Dynamics](./attachments/paper-list/1802.0317v1.pdf "vixra") - 待定
* [Lorentzian Distance Learning](./attachments/paper-list/BJe1hsCcYQ.pdf "ICLR 2019") - 待定
* [An Empirical Study on Post-processing Methods for Word Embeddings](./attachments/paper-list/1905.10971.pdf "arxiv") - 待定
* [Low-rank approximations of hyperbolic embeddings](./attachments/paper-list/1903.07307.pdf "arxiv") - 待定
* [Hyperbolic Image Embeddings](./attachments/paper-list/1904.02239.pdf "arxiv") - 待定
* [Hyperbolic Interaction Model For Hierarchical Multi-Label Classification](./attachments/paper-list/1905.10802.pdf "arxiv") - 待定
* [Adversarial autoencoders with constant-curvature latent manifolds](./attachments/paper-list/10.1016@j.asoc.2019.105511.pdf "Applied Soft Computing") - 待定
* [Scalable Hyperbolic Recommender Systems](./attachments/paper-list/1902.08648.pdf "arxiv") - 待定
* [Learning Distributed Representations of Symbolic Structure Using Binding and Unbinding Operations](./attachments/paper-list/1810.12456.pdf "arxiv") - 待定
* [Universal Transformers](./attachments/paper-list/1807.03819.pdf "arxiv") - 待定
* [Unsupervised learning by competing hidden units](./attachments/paper-list/10.1073@pnas.1820458116.pdf "pnas") - 待定

### 2019.6.9添加

* [EfficientNet: Rethinking Model Scaling for Convolutional Neural Networks](./attachments/paper-list/1905.11946.pdf "arxiv") - 待定

### 2019.6.8添加

* [The Neuro-Symbolic Concept Learner: Interpreting Scenes, Words, and Sentences From Natural Supervision](./attachments/paper-list/1904.12584.pdf "arxiv") - 待定
* [Liquid brains, solid brains](./attachments/paper-list/10.1098@rstb.2019.0040.pdf "Philosophical Transactions B") - 待定
* [Learn to Grow: A Continual Structure Learning Framework for Overcoming Catastrophic Forgetting](./attachments/paper-list/1904.00310.pdf "arxiv") - 待定

### 2019.6.6添加

* [The neural and computational bases of semantic cognition](./attachments/paper-list/10.1038@nrn.2016.150.pdf "REVIEWS") - 待定
* [Distinct roles of temporal and frontoparietal cortex in representing actions across vision and language](./attachments/paper-list/10.1038@s41467-018-08084-y.pdf "nature communications") - 待定
* [Brain Network Mechanisms of General Intelligence](./attachments/paper-list/657205.full.pdf "biorxiv") - 待定

### 2019.6.2添加

* [Deconstructing Lottery Tickets: Zeros, Signs, and the Supermask](./attachments/paper-list/1905.01067.pdf "arxiv") - 待定

### 2019.6.1添加

* [Dense Associative Memory is Robust to Adversarial Inputs](./attachments/paper-list/1701.00939.pdf "arxiv") - 待定

### 2019.5.31添加

* [Transferring Knowledge across Learning Processes](./attachments/paper-list/1812.01054.pdf "arxiv") - 待定
* [Data-Efficient Image Recognition with Contrastive Predictive Coding](./attachments/paper-list/1905.09272.pdf "arxiv") - 待定

### 2019.5.30添加

* [Communication dynamics in complex brain networks](./attachments/paper-list/10.1038@nrn.2017.149.pdf "review") - 待定
* [Ordered Neurons: Integrating Tree Structures into Recurrent Neural Networks](./attachments/paper-list/1810.09536.pdf "arxiv") - 待定

### 2019.5.29添加

* [An Explicitly Relational Neural Network Architecture](./attachments/paper-list/1905.10307.pdf "arxiv") - 待定
* [Brainstem nucleus incertus controls contextual memory formation](./attachments/paper-list/10.1126@science.aaw0445.pdf "Science") - 待定

### 2019.5.27添加

* [Robust associative learning is sufficient to explain structural and dynamical properties of local cortical circuits](./attachments/paper-list/320432.full.pdf "biorxiv") - 待定
* [Toward Identifying the Systems-Level Primitives of Cortex by In-Circuit Testing](./attachments/paper-list/10.3389@fncir.2018.00104.pdf "Front. Neural Circuits") - 待定

### 2019.5.24添加

* [Dorsal Raphe Serotonergic Neurons Control Intertemporal Choice under Trade-off](./attachments/paper-list/10.1016@j.cub.2017.09.008.pdf "Current Biology") - 待定

### 2019.5.22添加

* [High-dimensional geometry of population responses in visual cortex](./attachments/paper-list/374090.full.pdf "biorxiv") - 待定
* [Spontaneous behaviors drive multidimensional, brain-wide activity](./attachments/paper-list/306019.full.pdf "biorxiv") - 待定
* [STDP-based spiking deep convolutional neural networks for object recognition](./attachments/paper-list/10.1016@j.neunet.2017.12.005.pdf "Neural Networks") - 待定
* [Time-Space, Spiking Neural Networks and Brain-Inspired Artificial Intelligence](./attachments/paper-list/978-3-662-57715-8.pdf "springer") - 待定
* [Going Deeper in Spiking Neural Networks: VGG and Residual Architectures](./attachments/paper-list/1802.02627v4.pdf "arxiv") - 待定
* [Moving beyond reward prediction errors](./attachments/paper-list/10.1038@s42256-019-0053-0.pdf "nature machine intelligence") - 待定
* [The timing of action determines reward prediction signals in identified midbrain dopamine neurons](./attachments/paper-list/10.1038@s41593-018-0245-7.pdf "nature neuroscience") - 待定
* [Dopamine transients are sufficient and necessary for acquisition of model-based associations](./attachments/paper-list/10.1038@nn.4538.pdf "nature neuroscience") - 待定
* [Dopamine Neurons Respond to Errors in the Prediction of Sensory Features of Expected Rewards](./attachments/paper-list/10.1016@j.neuron.2017.08.025.pdf "neuron") - 待定

### 2019.5.21添加

* [Dynamic Computation in Visual Thalamocortical Networks](./attachments/paper-list/10.3390@e21050500.pdf "Entropy") - 待定

### 2019.5.20添加

* [NetPyNE, a tool for data-driven multiscale modeling of brain circuits](./attachments/paper-list/10.7554@eLife.44494.pdf "eLife") - 待定
* [Controlling Complexity of Cerebral Cortex Simulations, II: Streamlined Microcircuits](./attachments/paper-list/10.1162@necoa01188.pdf "Neural Computation") - 待定
* [Controlling Complexity of Cerebral Cortex Simulations—I: CxSystem, a Flexible Cortical Simulation Framework](./attachments/paper-list/10.1162@neco_a_01120.pdf "Neural Computation") - 待定
* [Brian2GeNN: a system for accelerating a large variety of spiking neural networks with graphics hardware](./attachments/paper-list/448050.full.pdf "biorxiv") - 待定
* [Acceleration of spiking neural networks in emerging multi-core and GPU architectures](./attachments/paper-list/10.1109@IPDPSW.2010.5470899.pdf "IPDPSW") - 待定
* [Brian 2: an intuitive and efficient neural simulator](./attachments/paper-list/595710.full.pdf "biorxiv") - 待定
* [CARLsim 4: An Open Source Library for Large Scale, Biologically Detailed Spiking Neural Network Simulation using Heterogeneous Clusters](./attachments/paper-list/10.1109@IJCNN.2018.8489326.pdf "biorxiv") - 待定
* [Conjunctive Coding in an Evolved Spiking Model of Retrosplenial Cortex](./attachments/paper-list/10.1037@bne0000236.pdf "Behav Neurosci") - 待定
* [Random neuronal ensembles can inherently do context dependent coarse conjunctive encoding of input stimulus without any specific training](./attachments/paper-list/10.1038@s41598-018-19462-3.pdf "Scientific Reports") - 待定
* [Evolving Spiking Neural Networks for Nonlinear Control Problems](./attachments/paper-list/1903.01180.pdf "arxiv") - 待定
* [Deep learning in spiking neural networks](./attachments/paper-list/10.1016@j.neunet.2018.12.002.pdf "Neural Networks") - 待定
* [Continual lifelong learning with neural networks: A review](./attachments/paper-list/10.1016@j.neunet.2019.01.012.pdf "Neural Networks") - 待定
* [Deep neural networks with weighted spikes](./attachments/paper-list/10.1016@j.neucom.2018.05.087.pdf "Neurocomputing") - 待定
* [Spiking neurons with short-term synaptic plasticity form superior generative networks](./attachments/paper-list/10.1038@s41598-018-28999-2.pdf "Scientific Reports") - 待定
* [A Supervised Learning Algorithm for Learning Precise Timing of Multiple Spikes in Multilayer Spiking Neural Networks](./attachments/paper-list/10.1109@TNNLS.2018.2797801.pdf "IEEE TRANSACTIONS ON NEURAL NETWORKS AND LEARNING SYSTEMS") - 待定

### 2019.5.17添加

* [Embedding Text in Hyperbolic Spaces](./attachments/paper-list/1806.04313.pdf "arxiv") - 待定
* [Neural Relational Inference for Interacting Systems](./attachments/paper-list/1802.04687.pdf "arxiv") - 待定
* [Human few-shot learning of compositional instructions](./attachments/paper-list/1901.04587.pdf "arxiv") - 待定
* [Analogues of mental simulation and imagination in deep learning](./attachments/paper-list/10.1016@j.cobeha.2018.12.011.pdf "ScienceDirect") - 待定
* [Reconciling deep learning with symbolic artificial intelligence: representing objects and relations](./attachments/paper-list/10.1016@j.cobeha.2018.12.010.pdf "ScienceDirect") - 待定
* [Doing more with less: meta-reasoning and meta-learning in humans and machines](./attachments/paper-list/10.1016@j.cobeha.2019.01.005.pdf "ScienceDirect") - 待定
* [On the necessity of abstraction](./attachments/paper-list/10.1016@j.cobeha.2018.11.005.pdf "ScienceDirect") - 待定
* [Distributed semantic representations for modeling human judgment](./attachments/paper-list/10.1016@j.cobeha.2019.01.020.pdf "ScienceDirect") - 待定
* [Linking neural responses to behavior with information-preserving population vectors](./attachments/paper-list/10.1016@j.cobeha.2019.03.004.pdf "ScienceDirect") - 待定

### 2019.5.16添加

* [Synaptic mechanisms of context-dependent sensory responses in the hippocampus](./attachments/paper-list/624262.full.pdf "biorxiv") - 待定
* [Constant sub-second cycling between representations of possible futures in the hippocampus](./attachments/paper-list/528976.full.pdf "biorxiv") - 待定
* [Dorsal and ventral hippocampus engage opposing networks in the nucleus accumbens](./attachments/paper-list/604116.full.pdf "biorxiv") - 待定
* [A neural signature of pattern separation in the monkey hippocampus](./attachments/paper-list/10.1073@pnas.1900804116.pdf "pnas") - 待定
* [Neural population dynamics in prefrontal cortex and hippocampus during paired-associate learning](./attachments/paper-list/578849.full.pdf "biorxiv") - 待定
* [The geometry of abstraction in hippocampus and pre-frontal cortex](./attachments/paper-list/408633.full.pdf "bioarxiv") - 待定
* [Focusing on what matters: Modulation of the human hippocampus by relational attention](./attachments/paper-list/446443.full.pdf "biorxiv") - 待定
* [An active inference model of concept learning](./attachments/paper-list/633677.full.pdf "bioarxiv") - 待定
* [Awake hippocampal-prefrontal replay mediates spatial learning and decision making](./attachments/paper-list/632042.full.pdf "biorxiv") - 待定
* [Fast and Flexible Sequence Induction In Spiking Neural Networks Via Rapid Excitability Changes](./attachments/paper-list/494310.full.pdf "biorxiv") - 待定
* [Computing Hubs in the Hippocampus and Cortex](./attachments/paper-list/513424.full.pdf "biorxiv") - 待定
* [Are place cells just memory cells? Memory compression leads to spatial tuning and history dependence](./attachments/paper-list/624239.full.pdf "biorxiv") - 待定
* [Acquisition of Temporal Order Involves a Reverberating Network in Hippocampal Field CA3](./attachments/paper-list/623025.full.pdf "biorxiv") - 待定
* [Graded striatal learning factors enable switches between goal-directed and habitual modes, by reassigning behavior control to the fastest-computed representation that predicts reward](./attachments/paper-list/619445.full.pdf "biorxiv") - 待定
* [An Indexing Theory for Working Memory based on Fast Hebbian Plasticity](./attachments/paper-list/334821.full.pdf "biorxiv") - 待定
* [Conditions for synaptic specificity during the maintenance phase of synaptic plasticity](./attachments/paper-list/617928.full.pdf "biorxiv") - 待定
* [A comprehensive characterization of rhythmic spiking activity in the rat ventral striatum](./attachments/paper-list/617233.full.pdf "biorxiv") - 待定
* [Population imaging of neural activity in awake behaving mice in multiple brain regions](./attachments/paper-list/616094.full.pdf "biorxiv") - 待定
* [Cell-type and endocannabinoid specific synapse connectivity in the adult nucleus accumbens core](./attachments/paper-list/613497.full.pdf "biorxiv") - 待定
* [The statistical structure of the hippocampal code for space as a function of time, context, and value](./attachments/paper-list/615203.full.pdf "biorxiv") - 待定
* [Dynamic compression and expansion in a classifying recurrent network](./attachments/paper-list/564476.full.pdf "biorxiv") - 待定

### 2019.5.15添加

* [A Unified Theory of Early Visual Representations from Retina to Cortex through Anatomically Constrained Deep CNNs](./attachments/paper-list/1901.00945.pdf "arxiv") - 待定
* [Biologically-Plausible Learning Algorithms Can Scale to Large Datasets](./attachments/paper-list/1811.03567.pdf "arxiv") - 待定
* [Decoupled Neural Interfaces using Synthetic Gradients](./attachments/paper-list/1608.05343.pdf "arxiv") - 待定

### 2019.5.13添加

* [Few-Shot Text Classification with Induction Network](./attachments/paper-list/1902.10482.pdf "arxiv") - 待定
* [Common features in plastic changes rather than constructed structures in recurrent neural network prefrontal cortex models](./attachments/paper-list/181297.full.pdf "biorxiv") - 待定
* [Neural Interactome: Interactive Simulation of a Neuronal System](./attachments/paper-list/10.3389@fncom.2019.00008.pdf "frontiers in computational neuroscience") - 待定
* [Prefrontal cortex creates novel navigation sequences from hippocampal place-cell replay with spatial reward propagation](./attachments/paper-list/466920.full.pdf "biorxiv") - 待定
* [Unsupervised Discovery of Demixed, Low-Dimensional Neural Dynamics across Multiple Timescales through Tensor Component Analysis](./attachments/paper-list/10.1016@j.neuron.2018.05.015.pdf "neuron") - 待定
* [NFTsim: Theory and Simulation of Multiscale Neural Field Dynamics](./attachments/paper-list/10.1371@journal.pcbi.1006387.pdf "PLOS computational biology") - 待定
* [Neural Classifiers with Limited Connectivity and Recurrent Readouts](./attachments/paper-list/10.1523@JNEUROSCI.3506-17.2018.pdf "jneurosci") - 待定
* [Layer 5 circuits in V1 differentially control visuomotor behavior](./attachments/paper-list/540807.full.pdf "biorxiv") - 待定
* [A comprehensive data-driven model of cat primary visual cortex](./attachments/paper-list/416156.full.pdf "biorxiv") - 待定

### 2019.5.12添加

* [Generalization as diffusion: human function learning on graphs](./attachments/paper-list/10.1101@538934.pdf "biorxiv") - 待定
* [Simulating bout-and-pause patterns with reinforcement learning](./attachments/paper-list/10.1101@632745.pdf "biorxiv") - 待定

### 2019.5.11添加

* [A Tri-network Model of Human Semantic Processing](./attachments/paper-list/10.3389@fpsyg.2017.01538.pdf "frontiers in psychology") - 待定
* [Rich-club connectivity, diverse population coupling, and dynamical activity patterns emerging from local cortical circuits](./attachments/paper-list/10.1371@journal.pcbi.1006902.pdf "computational biology") - 待定

### 2019.5.10添加

* [Evidence that recurrent circuits are critical to the ventral stream’s execution of core object recognition behavior](./attachments/paper-list/10.1038@s41593-019-0392-5.pdf "nature neuroscience") - 待定
* [Neural population control via deep image synthesis](./attachments/paper-list/10.1126@science.aav9436.pdf "science") - 待定
* [Training recurrent networks to generate hypotheses about how the brain solves hard navigation problems](./attachments/paper-list/1609.09059.pdf "arxiv") - 待定
* [Re-evaluating Circuit Mechanisms Underlying Pattern Separation](./attachments/paper-list/10.1016@j.neuron.2019.01.044.pdf "neuron") - 待定
* [Signatures and mechanisms of low-dimensional neural predictive manifolds](./attachments/paper-list/471987.full.pdf "bioRxiv") - 待定

### 2019.5.8添加

* [Efficient coding of subjective value](./attachments/paper-list/10.1038@s41593-018-0292-0.pdf "nature neuroscience") - 待定
* [Tracking the flow of hippocampal computation: Pattern separation, pattern completion, and attractor dynamics](./attachments/paper-list/10.1016@j.nlm.2015.10.008.pdf "Neurobiology of Learning and Memory") - 待定
* [Diversity in neural firing dynamics supports both rigid and learned hippocampal sequences](./attachments/paper-list/10.1126@science.aad1935.pdf "science") - 待定

### 2019.5.6添加

* [Multi-Scale Dense Networks for Resource Efficient Image Classification](./attachments/paper-list/1703.09844.pdf "arxiv") - 待定
* [Squeeze-and-Excitation Networks](./attachments/paper-list/1709.01507.pdf "arxiv") - 待定
* [Macroscale cortical organization and a default-like apex transmodal network in the marmoset monkey](./attachments/paper-list/10.1038@s41467-019-09812-8.pdf "nature communications") - 待定

### 2019.5.5添加

* [Self-Attention Capsule Networks for Image Classification](./attachments/paper-list/1904.12483.pdf "arxiv") - 待定
* [Neural dynamics at successive stages of the ventral visual stream are consistent with hierarchical error signals](./attachments/paper-list/10.7554@eLife.42870.001.pdf "eLIFE") - 待定

### 2019.5.4添加

* [Reinforcement Learning, Fast and Slow](./attachments/paper-list/10.1016@j.tics.2019.02.006.pdf "Trends in Cognitive Sciences") - 待定
* [On the interaction of social affect and cognition: empathy, compassion and theory of mind](./attachments/paper-list/10.1016@j.cobeha.2017.07.010.pdf "Behavioral Sciences") - 待定

### 2019.5.2添加

* [Neural Logic Machines](./attachments/paper-list/1904.11694.pdf "arxiv") - 待定

### 2019.4.26添加

* [The number sense is an emergent property of a deep convolutional neural network trained for object recognition](./attachments/paper-list/609347.full.pdf "bioRxiv") - 待定

### 2019.4.24添加

* [Theories of Error Back-Propagation in the Brain](./attachments/paper-list/10.1016@j.tics.2018.12.005.pdf "Trends in Cognitive Sciences") - 待定

### 2019.4.23添加

* [Visual physiology of the layer 4 cortical circuit in silico](./attachments/paper-list/10.1371@journal.pcbi.1006535.pdf "PLOS Computational Biology") - 待定
* [The subiculum: Unique hippocampal hub and more](./attachments/paper-list/10.1016@j.neures.2018.08.002.pdf "Neuroscience Research") - 待定
* [BioNet: A Python interface to NEURON for modeling large-scale networks](./attachments/paper-list/10.1371@journal.pone.0201630.pdf "PLOS ONE") - 待定
* [A large-scale, standardized physiological survey reveals higher order coding throughout the mouse visual cortex](./attachments/paper-list/10.1101@359513.pdf "bioRxiv") - 待定
* [A Computational Analysis of the Function of Three Inhibitory Cell Types in Contextual Visual Processing](./attachments/paper-list/10.3389@fncom.2017.00028.pdf "frontiers in Computational Neuroscience") - 待定
* [A Spiking Neurocomputational Model of High-Frequency Oscillatory Brain Responses to Words and Pseudowords](./attachments/paper-list/10.3389@fncom.2016.00145.pdf "frontiers in Computational Neuroscience") - 待定
* [Cortical circuits implement optimal context integration](./attachments/paper-list/158360.full.pdf "bioRxiv") - 待定
* [Hippocampal Reactivation of Random Trajectories Resembling Brownian Diffusion](./attachments/paper-list/10.1016@j.neuron.2019.01.052.pdf "Neuron") - 待定

### 2019.4.20添加

* [The new genetics of intelligence](./attachments/paper-list/10.1038@nrg.2017.104.pdf "Nature Reviews Genetics") - 待定
* [A unified model of human semantic knowledge and its disorders](./attachments/paper-list/10.1038@s41562-016-0039.pdf "Nature Human Behaviour") - 待定
* [Brain connections of words, perceptions and actions: A neurobiological model of spatiotemporal semantic activation in the human cortex](./attachments/paper-list/10.1016@j.neuropsychologia.2016.07.004.pdf "Neuropsychologia") - 待定
* [Conceptual grounding of language in action and perception: a neurocomputational model of the emergence of category specificity and semantic hubs](./attachments/paper-list/10.1111@ejn.13145.pdf "European Journal of Neuroscience") - 待定
* [Exploring Randomly Wired Neural Networks for Image Recognition](./attachments/paper-list/1904.01569.pdf "arxiv") - 待定
* [GLoMo: Unsupervisedly Learned Relational Graphs as Transferable Representations](./attachments/paper-list/1806.05662.pdf "arxiv") - 待定
* [Functional alignment with anatomical networks is associated with cognitive flexibility](./attachments/paper-list/10.1038@s41562-017-0260-9.pdf "Nature Human Behaviour") - 沈新科/庞浩
* [SuperSpike: Supervised Learning in Multilayer Spiking Neural Networks](./attachments/paper-list/10.1162@neco_a_01086.pdf "Neural Computation") - 待定
* [Surrogate Gradient Learning in Spiking Neural Networks](./attachments/paper-list/1901.09948.pdf "arxiv") - 待定
* [Synaptic Plasticity Dynamics for Deep Continuous Local Learning](./attachments/paper-list/1811.10766.pdf "arxiv") - 待定

### 2019.4.17添加

* [Task representations in neural networks trained to perform many cognitive tasks](./attachments/paper-list/10.1038@s41593-018-0310-2.pdf "nature neuroscience") - 孙昱昊/刘祥根

### 2019.4.9添加

* [The Dance of the Interneurons: How Inhibition Facilitates Fast Compressible and Reversible Learning in Hippocampus](./attachments/paper-list/dance of interneurons.pdf "bioRxiv") - 郑浩

### 2019.4.8添加

* [Deep Learning: A Critical Appraisal](./attachments/paper-list/1801.00631.pdf "arxiv") - 待定
* [Foundations of human reasoning in the prefrontal cortex](./attachments/paper-list/10.1126@science.1252254.pdf "Science") - 沈新科

### 2019.4.7添加

* [Frontal Cortex and the Hierarchical Control of Behavior](./attachments/paper-list/10.1016@j.tics.2017.11.005.pdf "Trends in Cognitive Sciences") - 待定

### 2019.4.6添加

* [Neural dynamics at successive stages of the ventral visual stream are consistent with hierarchical error signals](./attachments/paper-list/elife-42870.pdf "eLIFE") - 孙昱昊
* [Reversible Inactivation of Different Millimeter-Scale Regions of Primate IT Results in Different Patterns of Core Object Recognition Deficits](./attachments/paper-list/10.1016@j.neuron.2019.02.001.pdf "Neuron") - 孙昱昊
* [Hyperbolic Attention Networks](./attachments/paper-list/1805.09786.pdf "arxiv") - 庞浩

### 2019.4.3添加

* [The contribution of the human posterior parietal cortex to episodic memory](./attachments/paper-list/10.1038-nrn.2017.6.pdf "Neuron Perspective") - 孙昱昊
* [Economic Choice as an Untangling of Options into Actions](./attachments/paper-list/j.neuron.2018.06.038.pdf "Neuron Perspective") - 待定
* [Neural dynamics and circuit mechanisms of decision-making](./attachments/paper-list/j.conb.2012.08.006.pdf "Neurobiology") - 孙昱昊
* [Mechanisms of hierarchical reinforcement learning in corticostriatal circuits 1: computational analysis](./attachments/paper-list/bhr114.pdf "Open") - 沈新科
* [A distributed, hierarchical and recurrent framework for reward-based choice](./attachments/paper-list/nrn.2017.7.pdf "nature neuraoscience") - 孙昱昊
* [The contribution of the human posterior parietal cortex to episodic memory](./attachments/paper-list/nrn.2017.6.pdf "nature neuraoscience") - 待定
* [Neural Computations Underlying Causal Structure Learning](./attachments/paper-list/7143.full.pdf "The Journal of Neuroscience") - 沈新科/张文博
* [Integrating Models of Interval Timing and Reinforcement Learning](./attachments/paper-list/j.tics.2018.08.004.pdf "Trends in Cognitive Sciences") - 张文博

### 2019.4.1添加

* [Relational inductive biases, deep learning, and graph networks](./attachments/paper-list/1806.01261.pdf "arxiv") - 待定

### 2019.3.31添加

* [Locomotion-dependent remapping of distributed cortical networks](./attachments/paper-list/10.1038@s41593-019-0357-8.pdf "nature neuroscience") - 待定
* [Reinforcement learning in artificial and biological systems](./attachments/paper-list/s42256-019-0025-4.pdf "nature machine intelligence") - 庞浩
* [Organizing principles of pulvino-cortical functional coupling in humans](./attachments/paper-list/s41467-018-07725-6.pdf "nature communications") - 沈新科
* [Modality-independent encoding of individual concepts in the left parietal cortex](./attachments/paper-list/j.neuropsychologia.2017.05.001.pdf "Neuropsychologia") - 张文博
* [The neuro-cognitive representations of symbols: the case of concrete words](./attachments/paper-list/j.neuropsychologia.2017.06.026.pdf "Neuropsychologia") - 张文博

