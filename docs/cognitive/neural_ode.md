# Neural ODE
=================

在最近结束的 NeruIPS 2018 中，来自多伦多大学的陈天琦等研究者成为最佳论文的获得者。他们提出了一种名为神经常微分方程(Neural ODE / ODEnet)的模型，这是新一类的深度神经网络。神经常微分方程不拘于对已有架构的修修补补，它完全从另外一个角度考虑如何以连续的方式借助神经网络对数据建模。

ResNet的残差模块可以表示为 $h_{t+1} = h_t + f(h_t, \theta_t)$，表示成差分形式即为$\frac{(h_{t+1} - h_t)}{1} = f(h_t, \theta_t)$。若我们在层级间插入更多的层，且最终趋向于添加了无穷层时，则该变换可以表示为$\frac{dh_t}{dt} = f(h(t),t,\theta)$。

我们将输入表示为$h_0$，对上述微分方程求积分即可得到输出$h_1$，此为一次前向传播过程。反向传播中，将a(t)定义为损失函数L对隐状态z(t)的导数，再将a(t)对t求导。求出a(t)和z(t)后，再将L对参数$\theta$求导，$\frac{dL}{d\theta} = \int_{t_0}^{t_1} a(t)^T\frac{\partial{f(z(t), t, \theta)}}{\partial\theta}dt$。

ODEnet的一大优点是前向传播中不需要保存隐变量的值，因此只有常数级的内存占用。此外，该模型可以很好地实现准确性和计算复杂度的平衡，可以根据不同的场景设定微分方程求解的精度。

作者在MNIST数据集上演示了ODEnet在参数量约为ResNet 1/3的情况下达到了相似的准确率，且准确率比参数量相当的单隐层神经网络高了四倍。

作者在流模型上展示了ODEnet可以便捷快速地计算变量代换定理，具体参见原文。

论文网址：
https://arxiv.org/abs/1806.07366

论文解读：
https://mp.weixin.qq.com/s?__biz=MzIwMTc4ODE0Mw==&mid=2247493935&idx=1&sn=5c0b0e5fd088c9e1ffcc0277385a3c1d&chksm=96ea36afa19dbfb948978ad092efe776771003e516ce13d295ab196170684a015b721b09f219&mpshare=1&scene=1&srcid=1229Q0c2rcIeNCYSL8km4yO9#rd