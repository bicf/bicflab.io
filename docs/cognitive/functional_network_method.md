# Methods of constructing functional network
=================

这里我们介绍三篇典型的构建脑功能网络的方法。尚不全，有待补充。

## Power, 2011, Neuron. Functional network organization of the human brain.
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3222858/

Citation: 1573

Meta-analysis of task-related fMRI signals -> 322 ROIs, take 151 non-overlapping.

Using rs-fcMRI to identify 254 ROIs, take 193 non-overlapping (Refer to Cohen [1], detecting sharp transition in correlation patterns).

Meta-analytic ROIs were preferred, and non-overlapping fc-mapping ROIs were then added. Finally define 264 putative functional areas.

## Yeo, 2011, J Neurophysiology. The organization of the human cerebral cortex estimated by intrinsic functional connectivity.
https://www.ncbi.nlm.nih.gov/pubmed/21653723

Citation: 2622

Select 1175 ROI vertices spaced 16mm apart. Compute correlations of all vertices (18715) to these ROIs.

Binarize the correlation by set top 10% to 1, the others to 0.

Clustering	(model the data with a von Mises-Fisher distribution)

## Yuan, 2018, NeuroImage. Spatio-temporal modeling of connectome-scale brain network interactions via time-evolving graphs.
https://www.ncbi.nlm.nih.gov/pubmed/29102809

Citation: 1

Sparse dictionary learning generates functional network templates (using data of four task-related fMRI).

Hierarchical affinity propagation clustering generates 54 (automatedly decided) group-wise functional networks.