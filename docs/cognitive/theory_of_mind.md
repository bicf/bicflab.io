**心智理论（thoery of mind, ToM）是一种能够理解自己以及周围人类的心理状态的能力，这些心理状态包括情绪、信仰、意图、欲望、假装与知识等。**

**这里介绍两篇Department of Brain and Cognitive Sciences, MIT教授Josh Tenenbaum组的工作。第一篇是CCN2018会议的一篇报告，第二篇是它所基于的2017年的Nature Human Behaviour的工作。**

=======================================================================

=======================================================================

# **A Generative Model of People’s Intuitive Theory of Emotions**
==============================================================

Sean Dae Houlihan (daeda@mit.edu), Max Kleiman-Weiner, Joshua B. Tenenbaum & Rebecca Saxe

MIT Department of Brain and Cognitive Sciences,

77 Massachusetts Ave., Cambridge, MA 02139 USA

2018 Conference on Cognitive and Computational Neuroscience

## **实验设计**
游戏：两个参与者，有一定金额的奖金池。参与者表面上都承诺会选择平分奖金，但实际可选择平分或偷走奖金。假如两方都选择偷走（DD），则两人均不能得到奖金。假如一方选择平分，一方选择偷走（CD or DC），则奖金全部归偷走的一方。假如两方都选择平分（CC），则两人平分奖金。

有一个观察者听实验过程的讲述（context），评估游戏者在游戏中的情绪。本文建模观察者对游戏者情绪的评估值。

## **模型**
基于context的情绪预测和基于受试者表情的情绪预测可能结果差别很大，在两种信息都有时，可能在某些情境下context信息占主导，另一些情境下表情信息占主导，某些情境下可能做出与单独使用任何信息均不相同的判断。

这篇文章将inverse planning扩展到情绪预测，先通过context, reactions反推beliefs, goals, values，再通过belief, goals, values前向预测emotions。    

![图1](./../../img/cognitive/Houlihan_2018/1.png "图1")
<center>**图1**：文章框架（这里只用了event context信息）</center>

![图2](./../../img/cognitive/Houlihan_2018/2.png "图2")
<center>**图2**：Inverse planning示例。有M, C, T, W四种食品车，但只有三个停车位（其中一个在墙的另一边，人必须绕过墙才能看到）。某天一人出门找食物的路径如图，可推测此人对食物的偏好（预期效能）为T > M > C,W，此人的预测误差PE < 0，此人的情绪为失望。</center>

效能估计的三个心理因素[2]：1. 获得金钱的多少；2. 优势不平等厌恶（自己得到的钱更多）；3. 劣势不平等厌恶（自己得到的钱更少）。假设观测者可以根据直觉捕捉游戏者的社会动机。模型中给这三个因素分配权重，得到做出一个action的utility。

![图3](./../../img/cognitive/Houlihan_2018/3.png "图3")
<center>**图3**：模型表示[1]。</center>

![图4](./../../img/cognitive/Houlihan_2018/4.png "图4")
<center>**图4**：Model inversion（贝叶斯公式）[1]。</center>

![图5](./../../img/cognitive/Houlihan_2018/5.png "图5")
<center>**图5**：实际观测值评分与模型参数示意。</center>

在公开游戏中，游戏者还会考虑他的reputation因素，该模型被拓展为二阶。这里AIA的weight较大，DIA的weight较小，因为游戏者更不愿take advantage of others in public games[3].
![图6](./../../img/cognitive/Houlihan_2018/6.png "图6")
<center>**图6**：Base+Reputation model。</center>

下面作者用预期效能、实际达到的效能等特征来预测情绪。这里情绪的预测是用这些特征的简单求和来实现的（没有用到上述模型拟合的结果）。
![图7](./../../img/cognitive/Houlihan_2018/7.png "图7")
<center>**图7**：用于情绪预测的特征。</center>

作者将情绪如下表示[4]。这里举四个例子：

joy: U[Money] (I win money) + PE[Money] (I get more money than expected) + U[AIA] (I don't exploit the other person)

guilt: max(-EU[repuMoney] - EU[repuAIA], 0) (others think I tried to take advantage of my oppenent)

disappointment: -PE[Money]

fury: -PE[Money] - PE[DIA] 

![图8](./../../img/cognitive/Houlihan_2018/7.png "图8")
<center>**图8**：模型拟合结果与观测者评估结果比较。</center>


口头报告视频链接：https://www.ccneuro.org/videos.asp

文章链接：https://ccneuro.org/showDoc.php?s=W&pn=1128

## **参考文献**
[1] Baker, C. L., Jara-Ettinger, J., Saxe, R., & Tenenbaum, J. B. (2017). Rational quantitative attribution of beliefs, desires and percepts in human mentalizing. Nature Human Behaviour, 1(4), 0064.

[2] Fehr, E., & Schmidt, K. M. (1999). A theory of fairness, competition, and cooperation. The quarterly journal of economics, 114(3), 817–868.

[3] Kleiman-Weiner, M., Shaw, A., & Tenenbaum, J. B. (2017). Constructing social preferences from anticipated judgments: When impartial inequity is fair and why? In Proceedings of the 39th annual conference of the cognitive science society

[4] Scherer, K. R., & Meuleman, B. (2013). Human emotion experiences can be predicted on theoretical grounds: evidence from verbal labeling. PloS one, 8(3), e58166.

## **评价**
这篇文章把inverse model for appraisal和通过appraisal编码情绪的前期研究整合在一起，但感觉还没有把两部分有机地结合起来。不过这两部分理论都值得调研一下。

## **附：情绪与认知评估的关系**
### **解释情绪与决策的Appraisal-tendency framework**
Lerner等人发现同一效价的不同情绪可能引发不同的决策机制，比如悲伤和恐惧。不同的情绪和不同的认知评估模式相关。愤怒的个体控制更强，而恐惧的处境控制（situational control）更强。人们更倾向于在接下来的决策中延续导致当前情绪的认知评估（appraisal）。愤怒的人倾向于低估风险，而恐惧的人倾向于高估风险。

另一方面，思考的深度也受情绪影响。处于确定性更强的情绪（高兴、生气、恶心）中的人更依赖于启发式处理（依赖于expertise, attractiveness等线索），而处于确定性较弱情绪（悲伤）中的人更依赖于系统性处理（信息的质量和内容），但系统性处理的效果并不一定比启发式处理更好。

情绪还会影响内隐目标。例如处于焦虑中的人会寻求减少不确定性，悲伤的人会寻求奖赏。这样的内隐目标会影响他们的决策。

（认知评估是指个体对环境中刺激的主观解释。其维度包括不确定性、愉悦感、注意力活动、控制、预期的努力、自我和他人的责任等）

### **Friston's theory about emotion**
Free energy principle简介: 脑中有一个generative model，生成对世界的预测。假如这一预测和现实世界实际输入有偏差，则脑会不断更正模型，减小预测偏差（surprise）。模型输出的期望由突触活动决定，模型输出的不确定性则由活动的调弱或放大来决定。

Active inference理论认为，脑可以选择性地从与它的预测相符的数据中采样。脑还可以通过行动主动地改变外界环境来减小预测误差（allostasis）。

情绪反映了行为结果的不确定性的变化。正向情绪对应于减少不确定性的事件。

mood是不确定性的先验（long-term average）。Depression, mania（过度狂热）, anxiety等疾病可以纳入这个理论。

评价：可以认为Friston仅仅考虑了认知评估的一个维度（不确定性）与情绪的关系，但与情绪相关的因素远不止不确定性一种。



========================================================================

========================================================================

# **Rational quantitative attribution of beliefs, desires and percepts in human mentalizing**
========================================================

Chris L. Baker, Julian Jara-Ettinger, Rebecca Saxe and Joshua B. Tenenbaum*

Department of Brain and Cognitive Sciences, Massachusetts Institute of Technology, 77 Massachusetts Avenue, Cambridge, Massachusetts 02139, USA

Nature Human Behaviour, 2017

**“非理性理论是如此反直觉。因为它的反直觉，说明我们直觉中的心智理论（如果不是所有人类心智）在核心上是理性的。”**

**"the fact of their counter-intuitiveness may be the best evidence we have that intuitive theories of minds — if not always actual human minds — are rational to the core."**

##**Bayesian theory of mind (BToM) model:**
![图1](./../../img/cognitive/Baker_2017/1.png "图1")
<center>**图1**：Framework of the model. </center>


<img src="./../../img/cognitive/Baker_2017/2.png"  width="120"> 
<img src="./../../img/cognitive/Baker_2017/2_1.png"  width="400"> 
<center>**图2**：Bayes rule. B: beliefs, D: desires, P: perceptions, S: states, A: actions. $Pr(P|S)$代表观测者对agent在给定state下能看到什么的推测。$Pr(B_1|P,B_0)$代表观测者对于agent belief update的模型。$Pr(A|B_1, D)$代表观测者对于agent有效planning action的模型。 </center>

三种对比模型
lesion models:
	1. 忽略感知和推断能力以及它们之间的交互
	2. 认为actions是不会产生cost的
Model-free approach:
    3. 根据motion线性拟合desires和beliefs

##**Experiment 1**
有两个食品车位。Agent选择一个食品车进食。观测者和模型需根据agent的行动路线推测他更想吃那种食品（desire），以及他相信障碍物另一侧食品车是什么（belief）。

![图3](./../../img/cognitive/Baker_2017/3.png "图3")
<center>**图3**：实验1的情境与模型拟合结果。 </center>

##**Experiement 2**
有三个盒子，分别放在a图所示W,N,E的位置。agent prefers A>B>C。A, B盒子可能是关上的（没有reward），C盒子始终是打开的。观测者和模型需要根据agent的路线推测三个盒子各自的位置。

![图4](./../../img/cognitive/Baker_2017/4.png "图4")
<center>**图4**：实验2的情境与模型拟合结果。 </center>

这是一个比较难的问题，有较多的不确定性。从结果来看模型比人的选择更理性（comment by sxk）。

##**Discussion**
这里作者认为模拟的是infant“core mentalizing”的过程，即只是建立在最初的world representation上，以及对于其他人的metarepresentation。这种过程没有包含经验中形成的knowledge（我的理解），可以看成是最早形成的人类知识。（但是文中的实验用的是成年受试）

作者认为人的直觉中的心智理论是理性的。因为人类进化中最早的行为就是规划路径、寻找reward，这是人类很擅长的行为，可以认为人类在这一行为中是理性的。而延展到其他领域，人类可能就没有那么理性，但在这些领域中人们可能依然认为他人的行为应该是理性的。

##**Model details:**
###**1. Rantional agent model: POMDP (partially observed Marcov decision process)**
State space包含两类：agent所处的state (fully observed)、world state (partially observed)。

Desire: reward values.

Percepts: encode which world the agent can see (isovist).

Belief: a probability distribution over y (world state space), i.e., the probable reward place.

Belief and desire priors are discretized (grid). Belief is updated according to Bayes rule shown above.

Planning algorithm: policy is stochastic given by softmax of the look-ahead state-action value function.
![图5](./../../img/cognitive/Baker_2017/5.png "图5")  $\beta$: the hyperparameter to fit.
 
###**2. Rantional observer model: approximate Bayesian inference over rational agent moel**

The algorithm for inferring belief and reward is like forward-backward algorithm in HMMs.

![图6](./../../img/cognitive/Baker_2017/6.png "图6")

实验2中推断world state的方法：
![图7](./../../img/cognitive/Baker_2017/7.png "图7")

##**评价：**
这篇文章用贝叶斯框架，使得模型自然地能够通过agent的行为推理其belief, desire和percepts，对于理解人类行为和理解人类如何推测他人想法有重要意义。但其对理性观的解释值得讨论。

文章链接：https://www.nature.com/articles/s41562-017-0064