## 孙昱昊负责维护

##主要想法：

### Ideas：
- Graph: a general way to describe organization of input data.
- Graph Networks recieve graph organized inputs, cerebral cortex recieve certain kind of graph organized inputs, like visual.
- Graph Network computes different graphs under same framework, cerebral cortex computes different inputs under similar cortical circuits.

### Methods:
Structure -> Coding -> Dynamics -> Task -> Learning&Memory

### Research steps:
- Build a rate model with no graph representation, try different learning algorithms, make the system work.
- Try to add recurrent, interneurons, inhibitory neurons based on circuit level experiments. Notice: add graph based prior instead of pure visual prior.
- Try to add more features of cortex, including columns, and non-random connections.
- Try tasks not visual, prove the model is a graph model.

##相关文献：


#### function of recurrent and feedback connection in vision
- Neural dynamics at successive stages of the ventral visual stream are consistent with hierarchical error signals
- Evidence that recurrent circuits are critical to the ventral streams execution of core object recognition behavior	

#### relevant task
-  Non-local 
-  rule learning chemistry
-  IQ test raven matrix

#### model of local cortical circuit
- Cortical circuits implement optimal context integration
- Visual physiology of the layer 4 cortical circuit in silico
- A Computational Analysis of the Function of Three Inhibitory Cell Types in Contextual Visual Processing
- Recptive Fields Binocular Interaction and Function Architecture in the Cat's Visual Cortex

#### 相关实验
- Highly Non-random Features of Synaptic connectivity in Local Cortical Circuits
- Reversible Inactivation of Different Millimeter Scale Regions of Primate IT Results in Different Patterns of Core Object
- A Sparse Object Coding Scheme in Area V4

#### capsule network
- Dynamic Routing Between Capsules

#### Continuous Local Learning
- Synaptic Plasticity Dynamics for Deep Continuous Local Learning
- Theories of Error Back-Propagation in the Brain 
- SuperSpike: Supervised Learning in Multilayer Spiking Neural Networks 
- Surrogate Gradient Learning in Spiking Neural Networks 
- Neural Ordinary Differential Equations
